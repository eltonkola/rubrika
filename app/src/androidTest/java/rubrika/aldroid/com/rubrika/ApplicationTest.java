package com.aldroid.rubrika;

import android.app.Application;
import android.content.Intent;
import android.net.Uri;
import android.test.ApplicationTestCase;

import com.aldroid.rubrika.model.EksportTemplate;
import com.aldroid.rubrika.model.EksportTipi;
import com.aldroid.rubrika.model.Kontakt;
import com.aldroid.rubrika.model.KontaktTelefoni;
import com.aldroid.rubrika.task.RenderTaskEvents;
import com.aldroid.rubrika.task.RubrikaToPdf;
import com.aldroid.rubrika.utils.Utils;

import java.util.ArrayList;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);


        ArrayList<Kontakt> kontaktet = new ArrayList<Kontakt>();
        Kontakt k1 = new Kontakt();
        k1.setEmri("Elton Kola");
        KontaktTelefoni tel1 = new KontaktTelefoni();
        tel1.setEmri("tel shpi");
        tel1.setNumri("3333796801");
        k1.getTelefonat().add(tel1);
        kontaktet.add(k1);


        EksportTemplate t1 = new EksportTemplate();
        t1.setEmri("aaa");
        t1.setAllDetails(false);
        t1.setDjathtas(true);
        t1.setTipi(EksportTipi.PDF_SIMPLE);

        RubrikaToPdf task = new RubrikaToPdf(getSystemContext(), "/sdcard/test.pdf", kontaktet, t1, new RenderTaskEvents() {
            @Override
            public void error() {
                Utils.log("error");
            }

            @Override
            public void krijuar(String file) {
                Utils.log("krijuar" + file);
                Intent intent = new Intent ("org.androidprinting.intent.action.PRINT");
                intent.setDataAndType(Uri.parse(file), "application/pdf");
                getApplication().startActivity(intent);
            }

            @Override
            public void progressHap() {
                Utils.log("progressHap");
            }

            @Override
            public void progressMbyll() {
                Utils.log("progressMbyll");
            }

            @Override
            public void progress(Integer val, Integer tot) {
                Utils.log("prog val:" + val + "/tot:" + tot);
            }
        });
        Utils.execute(task);

    }
}