package com.aldroid.rubrika.model;

/**
 * Created by Elton on 07/09/2014.
 */
public enum EksportTipi {

    PDF_SIMPLE, PDF_COMPLETE, XLS_SIMPLE, XLS_COMPLETE, CSV_SIMPLE, CSV_COMPLETE

}
