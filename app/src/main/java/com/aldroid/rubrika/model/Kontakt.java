package com.aldroid.rubrika.model;

import java.util.ArrayList;

/**
 * Created by Elton on 06/09/2014.
 */
public class Kontakt {

    private String id;
    private String emri;
    private String notex;
    private String emri_organizates;
    private String pozita_organizate;
    private String lookup_key;
    private String thumbnail_uri;
    private ArrayList<KontaktEmail> emailet = new ArrayList<KontaktEmail>();
    private ArrayList<KontaktTelefoni> telefonat = new ArrayList<KontaktTelefoni>();
    private ArrayList<KontaktAdresa> adresat = new ArrayList<KontaktAdresa>();
    private ArrayList<KontaktMessenger> messengeret = new ArrayList<KontaktMessenger>();

    public String getThumbnail_uri() {
        return thumbnail_uri;
    }

    public void setThumbnail_uri(String thumbnail_uri) {
        this.thumbnail_uri = thumbnail_uri;
    }

    public String getLookup_key() {
        return lookup_key;
    }

    public void setLookup_key(String lookup_key) {
        this.lookup_key = lookup_key;
    }

    public String getEmri_organizates() {
        return emri_organizates;
    }

    public void setEmri_organizates(String emri_organizates) {
        this.emri_organizates = emri_organizates;
    }

    public String getPozita_organizate() {
        return pozita_organizate;
    }

    public void setPozita_organizate(String pozita_organizate) {
        this.pozita_organizate = pozita_organizate;
    }



    public String getId() {
        return id;
    }

    public ArrayList<KontaktAdresa> getAdresat() {
        return adresat;
    }

    public void setAdresat(ArrayList<KontaktAdresa> adresat) {
        this.adresat = adresat;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmri() {
        return emri;
    }

    public void setEmri(String emri) {
        this.emri = emri;
    }

    public ArrayList<KontaktEmail> getEmailet() {
        return emailet;
    }

    public void setEmailet(ArrayList<KontaktEmail> emailet) {
        this.emailet = emailet;
    }

    public ArrayList<KontaktTelefoni> getTelefonat() {
        return telefonat;
    }

    public void setTelefonat(ArrayList<KontaktTelefoni> telefonat) {
        this.telefonat = telefonat;
    }

    public String getNotex() {
        return notex;
    }

    public ArrayList<KontaktMessenger> getMessengeret() {
        return messengeret;
    }

    public void setMessengeret(ArrayList<KontaktMessenger> messengeret) {
        this.messengeret = messengeret;
    }

    public void setNotex(String notex) {
        this.notex = notex;
    }
}
