package com.aldroid.rubrika.model;

/**
 * Created by Elton on 07/09/2014.
 */
public class EksportTemplate {

    private EksportTipi tipi;
    private String emri;
    private String pershkrimi1;
    private String pershkrimi2;
    private int imazhi;
    private boolean allDetails = false;
    private boolean djathtas;

    public EksportTemplate(){

    }
    public EksportTemplate(EksportTipi tipi, String emri, String pershkrimi1, String pershkrimi2, int imazhi, boolean allDetails, boolean djathtas) {
        this.tipi = tipi;
        this.emri = emri;
        this.pershkrimi1 = pershkrimi1;
        this.pershkrimi2 = pershkrimi2;
        this.imazhi = imazhi;
        this.allDetails = allDetails;
        this.djathtas=djathtas;
    }

    public boolean isDjathtas() {
        return djathtas;
    }

    public void setDjathtas(boolean djathtas) {
        this.djathtas = djathtas;
    }

    public EksportTipi getTipi() {
        return tipi;
    }

    public void setTipi(EksportTipi tipi) {
        this.tipi = tipi;
    }

    public String getEmri() {
        return emri;
    }

    public void setEmri(String emri) {
        this.emri = emri;
    }

    public String getPershkrimi1() {
        return pershkrimi1;
    }

    public void setPershkrimi1(String pershkrimi1) {
        this.pershkrimi1 = pershkrimi1;
    }

    public String getPershkrimi2() {
        return pershkrimi2;
    }

    public void setPershkrimi2(String pershkrimi2) {
        this.pershkrimi2 = pershkrimi2;
    }

    public int getImazhi() {
        return imazhi;
    }

    public void setImazhi(int imazhi) {
        this.imazhi = imazhi;
    }

    public boolean isAllDetails() {
        return allDetails;
    }

    public void setAllDetails(boolean allDetails) {
        this.allDetails = allDetails;
    }
}
