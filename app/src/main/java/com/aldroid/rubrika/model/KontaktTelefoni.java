package com.aldroid.rubrika.model;

/**
 * Created by Elton on 06/09/2014.
 */
public class KontaktTelefoni {

    private String emri;
    private String numri;

    public String getEmri() {
        return emri;
    }

    public void setEmri(String emri) {
        this.emri = emri;
    }

    public String getNumri() {
        return numri;
    }

    public void setNumri(String numri) {
        this.numri = numri;
    }
}
