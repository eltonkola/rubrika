package com.aldroid.rubrika.model;

/**
 * Created by Elton on 06/09/2014.
 */
public class KontaktAdresa {

    private String poBox;
    private String street;
    private String city;
    private String state;
    private String postalCode;
    private String country;
    private String type;

    public String getPoBox() {
        return poBox;
    }

    public void setPoBox(String poBox) {
        this.poBox = poBox;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getAdresax(){
        if( country!=null && state!=null && city!=null && street!=null && poBox!=null && postalCode!=null){
            return "" + country + " " + state + " " + city + " " + street + " " + poBox + " " + postalCode;
        }else  if( state!=null && city!=null && street!=null && poBox!=null && postalCode!=null){
            return " " + state + " " + city + " " + street + " " + poBox + " " + postalCode;
        }else if(  city!=null && street!=null && poBox!=null && postalCode!=null){
            return " " + city + " " + street + " " + poBox + " " + postalCode;
        }else  if(   street!=null && poBox!=null && postalCode!=null){
            return " " + street + " " + poBox + " " + postalCode;
        }else{
            return null;
        }
    }


}
