package com.aldroid.rubrika.model;

/**
 * Created by Elton on 06/09/2014.
 */
public class KontaktMessenger {

    private String emri;
    private String adresa;

    public String getEmri() {
        return emri;
    }

    public void setEmri(String emri) {
        this.emri = emri;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }
}
