package com.aldroid.rubrika;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.aldroid.rubrika.utils.Utils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class WebviewActivity extends ActionBarActivity {

    private WebView webview;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(getIntent().getStringExtra("titulli"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setIcon(R.drawable.logo);

         setContentView(R.layout.activity_webview);
        
        
        webview = (WebView)findViewById(R.id.webview);
        
        
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setBuiltInZoomControls(true);

        webview.getSettings().setSupportZoom(true);
                
        
        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                view.loadUrl(url);
                return false;
           }
        });
        
        webview.loadUrl(getIntent().getStringExtra("url"));
       
        
        // Create the adView
        if(Utils.showAds) {
            AdView mAdView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }else{
            findViewById(R.id.adView).setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroy() {
      super.onDestroy();
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
                finish();
        }
        return super.onOptionsItemSelected(item);
     }

}