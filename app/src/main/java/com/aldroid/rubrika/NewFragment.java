package com.aldroid.rubrika;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aldroid.rubrika.adapter.TemplateAdapter;
import com.aldroid.rubrika.model.EksportTemplate;
import com.aldroid.rubrika.model.EksportTipi;

/**
 * Created by Elton on 06/09/2014.
 */
public class NewFragment extends Fragment {

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static NewFragment newInstance() {
        NewFragment fragment = new NewFragment();
        return fragment;
    }

    public NewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new, container, false);



        // 1. get a reference to recyclerView
        RecyclerView recyclerView = (RecyclerView)rootView.findViewById(R.id.recyclerView);

        // this is data fro recycler view
        final EksportTemplate itemsData[] = {
                new EksportTemplate(EksportTipi.PDF_SIMPLE, getString(R.string.pdf_tit_1), getString(R.string.pdf_des_1), getString(R.string.pdf_txt_1),R.drawable.template_pdf_1, false, false),
                new EksportTemplate(EksportTipi.PDF_COMPLETE, getString(R.string.pdf_tit_2), getString(R.string.pdf_des_2), getString(R.string.pdf_txt_2),R.drawable.template_pdf_2, true, true),
                new EksportTemplate(EksportTipi.XLS_SIMPLE, getString(R.string.xls_tit_1), getString(R.string.xls_des_1), getString(R.string.xls_txt_1),R.drawable.template_xls_1, false, false),
                new EksportTemplate(EksportTipi.XLS_COMPLETE, getString(R.string.xls_tit_2), getString(R.string.xls_des_2), getString(R.string.xls_txt_2),R.drawable.template_xls_2, true, true),
                new EksportTemplate(EksportTipi.CSV_SIMPLE, getString(R.string.csv_tit_1), getString(R.string.csv_des_1), getString(R.string.csv_txt_1),R.drawable.template_csv_1, false, false),
                new EksportTemplate(EksportTipi.CSV_COMPLETE, getString(R.string.csv_tit_2), getString(R.string.csv_des_2), getString(R.string.csv_txt_2),R.drawable.template_csv_2, true, true)
                               };

        // 2. set layoutManger
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        // 3. create an adapter
        TemplateAdapter mAdapter = new TemplateAdapter(itemsData);

        mAdapter.setOnItemClickListener(new TemplateAdapter.OnItemClickListenerVk() {
            @Override
            public void onItemClick(View view, int position) {
                EksportTemplate tmpl = itemsData[position];
                KrijoActivity.template = tmpl;
                //Toast.makeText(getActivity(),"Template click:" + tmpl.getEmri() , Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getActivity(), KrijoActivity.class));
            }
        });

        // 4. set adapter
        recyclerView.setAdapter(mAdapter);
        // 5. set item animator to DefaultAnimator
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //((MainActivity) activity).onSectionAttached(1);
    }
}
