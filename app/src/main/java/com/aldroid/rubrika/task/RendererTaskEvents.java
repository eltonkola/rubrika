package com.aldroid.rubrika.task;

/**
 * Created by Elton on 06/09/2014.
 */
public interface RendererTaskEvents {
    public void progress(Integer val, Integer tot);
}
