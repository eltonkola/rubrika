package com.aldroid.rubrika.task;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;

import com.aldroid.rubrika.model.Kontakt;
import com.aldroid.rubrika.model.KontaktAdresa;
import com.aldroid.rubrika.model.KontaktEmail;
import com.aldroid.rubrika.model.KontaktMessenger;
import com.aldroid.rubrika.model.KontaktTelefoni;
import com.aldroid.rubrika.utils.Utils;

import java.util.ArrayList;

/**
 * Created by Elton on 06/09/2014.
 */
public class LoadKontaktet extends AsyncTask<Void, Integer, Boolean> {

    private Activity cntx;
    private ArrayList<Kontakt> kontaktet = new ArrayList<Kontakt>();
    private KontaktTaskEvents events;
    private boolean loadAll;

    public LoadKontaktet(Activity cntx, boolean loadAll, KontaktTaskEvents events){
        this.cntx=cntx;
        this.events=events;
        this.loadAll=loadAll;
    }


    @Override
    protected void onPreExecute() {
        kontaktet = new ArrayList<Kontakt>();
        if(events!=null)events.progressHap();
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {


            ContentResolver cr = cntx.getContentResolver();
            String[] kontaktProjektor = new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts.HAS_PHONE_NUMBER, ContactsContract.Contacts.LOOKUP_KEY, ContactsContract.Contacts.PHOTO_THUMBNAIL_URI};

            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, kontaktProjektor, null, null, null);
            int tot = cur.getCount();
            int punt = 0;
            if (tot > 0) {
                while (cur.moveToNext()) {
                    punt++;
                    publishProgress(punt, tot);
                    Kontakt kontakti =  new Kontakt();

                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    String lookup_key = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                    String thumbnail_uri = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI));


                    Utils.log("name : " + name + ", ID : " + id);
                    kontakti.setId(id);
                    kontakti.setEmri(name);
                    kontakti.setLookup_key(lookup_key);
                    kontakti.setThumbnail_uri(thumbnail_uri);

                    if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        String[] phoneProjektor = new String[]{ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.LABEL};
                        Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, phoneProjektor, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                        while (pCur.moveToNext()) {
                            int phoneType = pCur.getInt(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                            String phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            String label = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.LABEL));
                            Utils.log("phone" + phone);

                            KontaktTelefoni tel = new KontaktTelefoni();
                            tel.setEmri(getTelTipi(phoneType, label));
                            tel.setNumri(phone);

                            kontakti.getTelefonat().add(tel);

                        }
                        pCur.close();

                        if(loadAll) {
                            String[] emailProjektor = new String[]{ContactsContract.CommonDataKinds.Email.DATA, ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.LABEL};
                            Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, emailProjektor, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                            while (emailCur.moveToNext()) {
                                String email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                                int emailType = emailCur.getInt(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
                                String label = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.LABEL));

                                KontaktEmail emailx = new KontaktEmail();
                                emailx.setAdresa(email);
                                emailx.setEmri(getEmailType(emailType, label));

                                kontakti.getEmailet().add(emailx);


                                Utils.log("Email " + email + " Email Type : " + emailType);
                            }
                            emailCur.close();

                            String noteWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
                            String[] noteWhereParams = new String[]{id, ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE};
                            String[] noteProjektor = new String[]{ContactsContract.CommonDataKinds.Note.NOTE};
                            Cursor noteCur = cr.query(ContactsContract.Data.CONTENT_URI, noteProjektor, noteWhere, noteWhereParams, null);
                            if (noteCur.moveToFirst()) {
                                String note = noteCur.getString(noteCur.getColumnIndex(ContactsContract.CommonDataKinds.Note.NOTE));
                                Utils.log("Note " + note);
                                kontakti.setNotex(note);
                            }
                            noteCur.close();

                            String addrWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
                            String[] addrWhereParams = new String[]{id, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE};
                            String[] adresaProjektor = new String[]{ContactsContract.CommonDataKinds.StructuredPostal.POBOX, ContactsContract.CommonDataKinds.StructuredPostal.LABEL, ContactsContract.CommonDataKinds.StructuredPostal.STREET, ContactsContract.CommonDataKinds.StructuredPostal.CITY, ContactsContract.CommonDataKinds.StructuredPostal.REGION, ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE, ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY, ContactsContract.CommonDataKinds.StructuredPostal.TYPE};
                            Cursor addrCur = cr.query(ContactsContract.Data.CONTENT_URI, adresaProjektor, null, null, null);
                            while (addrCur.moveToNext()) {
                                String poBox = addrCur.getString(addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POBOX));
                                String street = addrCur.getString(addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
                                String city = addrCur.getString(addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
                                String state = addrCur.getString(addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.REGION));
                                String postalCode = addrCur.getString(addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE));
                                String country = addrCur.getString(addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));
                                int type = addrCur.getInt(addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.TYPE));
                                String label = addrCur.getString(addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.LABEL));

                                KontaktAdresa adresa = new KontaktAdresa();
                                adresa.setCity(city);
                                adresa.setCountry(country);
                                adresa.setPoBox(poBox);
                                adresa.setPostalCode(postalCode);
                                adresa.setState(state);
                                adresa.setStreet(street);
                                adresa.setType(getAdresaType(type, label));

                                if(adresa.getAdresax()!=null) {
                                    kontakti.getAdresat().add(adresa);
                                }
                            }
                            addrCur.close();

                            String imWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
                            String[] imWhereParams = new String[]{id, ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE};
                            String[] imProjektor = new String[]{ContactsContract.CommonDataKinds.Im.DATA, ContactsContract.CommonDataKinds.Im.TYPE, ContactsContract.CommonDataKinds.Im.LABEL};
                            Cursor imCur = cr.query(ContactsContract.Data.CONTENT_URI, imProjektor, imWhere, imWhereParams, null);
                            if (imCur.moveToFirst()) {
                                String imName = imCur.getString(imCur.getColumnIndex(ContactsContract.CommonDataKinds.Im.DATA));
                                int imType = imCur.getInt(imCur.getColumnIndex(ContactsContract.CommonDataKinds.Im.TYPE));
                                String label = imCur.getString(imCur.getColumnIndex(ContactsContract.CommonDataKinds.Im.LABEL));

                                KontaktMessenger mes = new KontaktMessenger();
                                mes.setAdresa(imName);
                                mes.setEmri(getImType(imType, label));

                                kontakti.getMessengeret().add(mes);
                            }
                            imCur.close();


                            String orgWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
                            String[] orgWhereParams = new String[]{id, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE};
                            String[] orgProjektor = new String[]{ContactsContract.CommonDataKinds.Organization.DATA, ContactsContract.CommonDataKinds.Organization.TITLE};
                            Cursor orgCur = cr.query(ContactsContract.Data.CONTENT_URI, orgProjektor, orgWhere, orgWhereParams, null);
                            if (orgCur.moveToFirst()) {
                                String orgName = orgCur.getString(orgCur.getColumnIndex(ContactsContract.CommonDataKinds.Organization.DATA));
                                String title = orgCur.getString(orgCur.getColumnIndex(ContactsContract.CommonDataKinds.Organization.TITLE));


                                kontakti.setEmri_organizates(orgName);
                                kontakti.setPozita_organizate(title);
                            }
                            orgCur.close();
                        }

                        kontaktet.add(kontakti);
                    }

                }
            }

        return true;
    }


    private String getImType(int tipi, String label){
        switch (tipi){
            case  ContactsContract.CommonDataKinds.Im.TYPE_CUSTOM:
            if(label==null){
                return "Custom";
            }else {
                return label;
            }
            case ContactsContract.CommonDataKinds.Im.TYPE_HOME:
                return "Home";
            case ContactsContract.CommonDataKinds.Im.TYPE_OTHER:
                return "Other";
            case ContactsContract.CommonDataKinds.Im.TYPE_WORK:
                return "Work";
            default:
                return "?";
        }
    }

    private String getAdresaType(int tipi, String label){
        switch (tipi){
            case  ContactsContract.CommonDataKinds.StructuredPostal.TYPE_CUSTOM:
                if(label==null){
                    return "Custom";
                }else {
                    return label;
                }
            case ContactsContract.CommonDataKinds.StructuredPostal.TYPE_HOME:
                return "Home";
            case ContactsContract.CommonDataKinds.StructuredPostal.TYPE_OTHER:
                return "Other";
            case ContactsContract.CommonDataKinds.StructuredPostal.TYPE_WORK:
                return "Work";
            default:
                return "?";
        }
    }


    private String getEmailType(int tipi, String label){
        switch (tipi){
            case  ContactsContract.CommonDataKinds.Email.TYPE_CUSTOM:
                if(label==null){
                    return "Custom";
                }else {
                    return label;
                }
            case ContactsContract.CommonDataKinds.Email.TYPE_HOME:
                return "Home";
            case ContactsContract.CommonDataKinds.Email.TYPE_MOBILE:
                return "Mobile";
            case ContactsContract.CommonDataKinds.Email.TYPE_OTHER:
                return "Other";
            case ContactsContract.CommonDataKinds.Email.TYPE_WORK:
                return "Work";
            default:
                return "?";
        }
    }

    private String getTelTipi(int tipi, String label){
        switch (tipi){
            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                return "Home";
            case ContactsContract.CommonDataKinds.Phone.TYPE_ASSISTANT:
                return "Assistant";
            case ContactsContract.CommonDataKinds.Phone.TYPE_CALLBACK:
                return "Callback";
            case ContactsContract.CommonDataKinds.Phone.TYPE_CAR:
                return "Car";
            case ContactsContract.CommonDataKinds.Phone.TYPE_COMPANY_MAIN:
                return "Company Main";
            case ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM:
                if(label==null){
                    return "Custom";
                }else {
                    return label;
                }
            case ContactsContract.CommonDataKinds.Phone.TYPE_FAX_HOME:
                return "Fax Home";
            case ContactsContract.CommonDataKinds.Phone.TYPE_FAX_WORK:
                return "Fax Work";
            case ContactsContract.CommonDataKinds.Phone.TYPE_ISDN:
                return "Isdn";
            case ContactsContract.CommonDataKinds.Phone.TYPE_MAIN:
                return "Main";
            case ContactsContract.CommonDataKinds.Phone.TYPE_MMS:
                return "Mms";
            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                return "Mobile";
            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                return "Other";
            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER_FAX:
                return "Fax";
            case ContactsContract.CommonDataKinds.Phone.TYPE_RADIO:
                return "Radio";
            case ContactsContract.CommonDataKinds.Phone.TYPE_PAGER:
                return "Pager";
            case ContactsContract.CommonDataKinds.Phone.TYPE_TELEX:
                return "Telex";
            case ContactsContract.CommonDataKinds.Phone.TYPE_TTY_TDD:
                return "Tty Tdd";
            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                return "Work";
            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE:
                return "Mobile";
            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_PAGER:
                return "Work Pager";
            default:
                return "?";
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if(events!=null)events.progress(values[0], values[1]);
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if(events!=null)events.progressMbyll();
        if(aBoolean){
            if(events!=null)events.karikim(kontaktet);
        }else{
            if(events!=null)events.error();;
        }

        super.onPostExecute(aBoolean);
    }
}
