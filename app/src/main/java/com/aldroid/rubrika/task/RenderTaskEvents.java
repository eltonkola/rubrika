package com.aldroid.rubrika.task;

/**
 * Created by Elton on 06/09/2014.
 */
public interface RenderTaskEvents {

    public void error();
    public void krijuar(String file);
    public void progressHap();
    public void progressMbyll();
    public void progress(Integer val, Integer tot);

}
