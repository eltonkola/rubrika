package com.aldroid.rubrika.task;

import com.aldroid.rubrika.model.Kontakt;

import java.util.ArrayList;

/**
 * Created by Elton on 06/09/2014.
 */
public interface KontaktTaskEvents {

    public void error();
    public void karikim(ArrayList<Kontakt> kontaktet);
    public void progressHap();
    public void progressMbyll();
    public void progress(Integer val, Integer tot);

}
