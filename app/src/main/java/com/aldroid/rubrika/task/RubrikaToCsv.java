package com.aldroid.rubrika.task;

import android.content.Context;
import android.os.AsyncTask;

import com.aldroid.rubrika.model.EksportTemplate;
import com.aldroid.rubrika.model.EksportTipi;
import com.aldroid.rubrika.model.Kontakt;
import com.aldroid.rubrika.renderer.CsvComplete;
import com.aldroid.rubrika.renderer.CsvSimple;

import java.util.ArrayList;

/**
 * Created by Elton on 06/09/2014.
 */
public class RubrikaToCsv extends AsyncTask<Void,Integer, Boolean>{

    private Context cntx;
    private String path;
    private ArrayList<Kontakt> kontaktet = new ArrayList<Kontakt>();
    private EksportTemplate template;
    private RenderTaskEvents events;

    public RubrikaToCsv(Context cntx, String path, ArrayList<Kontakt> kontaktet, EksportTemplate template, RenderTaskEvents events){
        this.cntx=cntx;
        this.path=path;
        this.kontaktet=kontaktet;
        this.template=template;
        this.events=events;
    }

    private RendererTaskEvents progEvent = new RendererTaskEvents() {
        @Override
        public void progress(Integer val, Integer tot) {
            publishProgress(val, tot);
        }
    };


    @Override
    protected void onPreExecute() {
        if(events!=null)events.progressHap();
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        try {

            if(template.getTipi()== EksportTipi.CSV_SIMPLE) {
                new CsvSimple().createFile(path, kontaktet, progEvent, cntx);
            }else{
                new CsvComplete().createFile(path, kontaktet, progEvent, cntx);
            }

            return true;
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if(events!=null)events.progress(values[0], values[1]);
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if(events!=null)events.progressMbyll();
        if(aBoolean){
            if(events!=null)events.krijuar(path);
        }else{
            if(events!=null)events.error();
        }
        super.onPostExecute(aBoolean);
    }
}
