package com.aldroid.rubrika;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.QuickContactBadge;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.aldroid.rubrika.adapter.AddressAdapter;
import com.aldroid.rubrika.adapter.ContactsAdapter;
import com.aldroid.rubrika.adapter.EmailAdapter;
import com.aldroid.rubrika.adapter.ImAdapter;
import com.aldroid.rubrika.adapter.PhoneAdapter;
import com.aldroid.rubrika.model.Kontakt;
import com.aldroid.rubrika.model.KontaktAdresa;
import com.aldroid.rubrika.model.KontaktEmail;
import com.aldroid.rubrika.model.KontaktMessenger;
import com.aldroid.rubrika.model.KontaktTelefoni;
import com.aldroid.rubrika.utils.Utils;

import java.util.ArrayList;

/**
 * Created by Elton on 26/09/2014.
 */
public class UserEditDialog extends Dialog{

    public UserEditDialog(Context context) {
        super(context);
        this.mContext=context;
        init();
    }

    public UserEditDialog(Context context, Kontakt kontakt, OnDialogEditEvent events) {
        super(context);
        this.mContext=context;
        this.kontakt=kontakt;
        this.events=events;
        init();
    }


    public UserEditDialog(Context context, int theme) {
        super(context, theme);
        this.mContext=context;
        init();
    }

    protected UserEditDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.mContext=context;
        init();
    }

    private Kontakt kontakt;

    private Context mContext;
    private RecyclerView recyclerView;
    private OnDialogEditEvent events;
    private Button buttonClose;
    private Button buttonDelete;
    private Button buttonSave;

    private Button tab1;
    private Button tab2;
    private Button tab3;
    private Button tab4;

    private PhoneAdapter phoneAdapter;
    private EmailAdapter emailAdapter;
    private AddressAdapter addressAdapter;
    private ImAdapter imAdapter;

    private TextView new_element;
    private QuickContactBadge ikona;
    private EditText emrix;
    private EditText pozita_organizate;
    private EditText emri_organizates;
    private EditText notex;

    private int currentTab = 1;

    private void init(){

        setTitle("Edit contact details");

        setContentView(R.layout.dialog_edit_user);

        ikona = (QuickContactBadge)findViewById(R.id.ikona);

        emrix = (EditText)findViewById(R.id.emri);
        pozita_organizate = (EditText)findViewById(R.id.pozita_organizate);
        emri_organizates = (EditText)findViewById(R.id.emri_organizates);
        notex = (EditText)findViewById(R.id.notex);

        emrix.setText(kontakt.getEmri());
        notex.setText(kontakt.getNotex());
        pozita_organizate.setText(kontakt.getPozita_organizate());
        emri_organizates.setText(kontakt.getEmri_organizates());


        final Uri contactUri = ContactsContract.Contacts.getLookupUri(Long.parseLong(kontakt.getId()) ,kontakt.getLookup_key());
        ikona.assignContactUri(contactUri);

        String photoUri = kontakt.getThumbnail_uri();

        Utils.log("-------------------------");
        Utils.log("getThumbnail_uri:" + photoUri);
        Utils.log("getLookup_key:" + kontakt.getLookup_key());
        Utils.log("getId:" + kontakt.getId());

//        final  QuickContactBadge ikona = holder.ikona;

        if(photoUri!=null) {
            Utils.log("contact foto:" + photoUri);
            ikona.setImageURI(Uri.parse(photoUri));
        }else{
            ikona.setImageResource(R.drawable.ic_launcher);
        }


        // 1. get a reference to recyclerView
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);

        // 2. set layoutManger
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.scrollToPosition(0);
        recyclerView.setLayoutManager(layoutManager);


        // 3. create an adapter
        phoneAdapter = new PhoneAdapter(kontakt.getTelefonat());
        phoneAdapter.setOnItemClickListener(new PhoneAdapter.OnItemClickListenerVk() {

            @Override
            public void onItemKlick(View view, int position) {
                editPhone(position);
            }

            @Override
            public void onItemDelete(int position) {
                kontakt.getTelefonat().remove(position);
                phoneAdapter.notifyItemRemoved(position);
            }
        });


        emailAdapter = new EmailAdapter(kontakt.getEmailet());
        emailAdapter.setOnItemClickListener(new EmailAdapter.OnItemClickListenerVk() {

            @Override
            public void onItemKlick(View view, int position) {
                KontaktEmail emaili = kontakt.getEmailet().get(position);
//                Toast.makeText(mContext, "TODO - edit:" + emaili.getEmri(), Toast.LENGTH_SHORT).show();
                editEmail(position);
            }

            @Override
            public void onItemDelete(int position) {
                kontakt.getEmailet().remove(position);
                emailAdapter.notifyItemRemoved(position);
            }
        });

        imAdapter = new ImAdapter(kontakt.getMessengeret());
        imAdapter.setOnItemClickListener(new ImAdapter.OnItemClickListenerVk() {

            @Override
            public void onItemKlick(View view, int position) {
                KontaktMessenger msg = kontakt.getMessengeret().get(position);
//                Toast.makeText(mContext, "TODO - edit:" + msg.getEmri(), Toast.LENGTH_SHORT).show();
                editIm(position);
            }

            @Override
            public void onItemDelete(int position) {
                kontakt.getMessengeret().remove(position);
                imAdapter.notifyItemRemoved(position);
            }
        });

        addressAdapter = new AddressAdapter(kontakt.getAdresat());
        addressAdapter.setOnItemClickListener(new AddressAdapter.OnItemClickListenerVk() {

            @Override
            public void onItemKlick(View view, int position) {
                KontaktAdresa adr = kontakt.getAdresat().get(position);
//                Toast.makeText(mContext, "TODO - edit:" + adr.getType(), Toast.LENGTH_SHORT).show();
                editAddress(position);
            }

            @Override
            public void onItemDelete(int position) {
                kontakt.getAdresat().remove(position);
                addressAdapter.notifyItemRemoved(position);
            }
        });


        // 4. set adapter
        recyclerView.setAdapter(phoneAdapter);
        // 5. set item animator to DefaultAnimator
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        buttonClose= (Button)findViewById(R.id.buttonClose);
        buttonDelete= (Button)findViewById(R.id.buttonDelete);
        buttonSave= (Button)findViewById(R.id.buttonSave);


        tab1 = (Button)findViewById(R.id.tab1);
        tab2 = (Button)findViewById(R.id.tab2);
        tab3 = (Button)findViewById(R.id.tab3);
        tab4 = (Button)findViewById(R.id.tab4);

        new_element= (TextView)findViewById(R.id.new_element);

        tab1.setOnClickListener(new TabKlik(1));
        tab2.setOnClickListener(new TabKlik(2));
        tab3.setOnClickListener(new TabKlik(3));
        tab4.setOnClickListener(new TabKlik(4));



        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserEditDialog.this.dismiss();
            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(events!=null)events.onDeleteKontakt(kontakt);
                UserEditDialog.this.dismiss();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ruajme tere fushat e modifikuara

                kontakt.setEmri(emrix.getText().toString());
                kontakt.setNotex(notex.getText().toString());
                kontakt.setPozita_organizate(pozita_organizate.getText().toString());
                kontakt.setEmri_organizates(emri_organizates.getText().toString());

                Utils.log("update kontakt end:" + kontakt);
                UserEditDialog.this.dismiss();
                if(events!=null)events.onEditKontakt(kontakt);

            }
        });


        new_element.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newElement();
            }
        });
    }

    private void editPhone(final int pos) {

        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

        alert.setTitle("Edit Phone Number");

        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popView = inflater.inflate(R.layout.dialog_emer_vlere, null);

        final EditText emri = (EditText)popView.findViewById(R.id.emri);
        final EditText vlera= (EditText)popView.findViewById(R.id.vlera);

        emri.setText(kontakt.getTelefonat().get(pos).getEmri());
        vlera.setText(kontakt.getTelefonat().get(pos).getNumri());

        alert.setView(popView);
        alert.setCancelable(true);
        alert.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(emri.getText().length()>0 && vlera.getText().length()>0){
                   kontakt.getTelefonat().get(pos).setEmri(emri.getText().toString());
                   kontakt.getTelefonat().get(pos).setNumri(vlera.getText().toString());
                   phoneAdapter.notifyItemChanged(pos);
                   phoneAdapter.notifyDataSetChanged();
                }else{
                    Toast.makeText(mContext, "Fill the form!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alert.show();

    }

    private void editEmail(final int pos) {

        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

        alert.setTitle("Edit Email");

        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popView = inflater.inflate(R.layout.dialog_emer_vlere, null);

        final EditText emri = (EditText)popView.findViewById(R.id.emri);
        final EditText vlera= (EditText)popView.findViewById(R.id.vlera);

        final TextView emri_txt = (TextView)popView.findViewById(R.id.emri_txt);
        final TextView vlera_txt = (TextView)popView.findViewById(R.id.vlera_txt);

        emri_txt.setText("Name");
        emri.setHint("Work/Persona/Other");
        vlera_txt.setText("Email");
        vlera.setHint("Email address");

        emri.setText(kontakt.getEmailet().get(pos).getEmri());
        vlera.setText(kontakt.getEmailet().get(pos).getAdresa());

        alert.setView(popView);
        alert.setCancelable(true);
        alert.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(emri.getText().length()>0 && vlera.getText().length()>0){
                    kontakt.getEmailet().get(pos).setEmri(emri.getText().toString());
                    kontakt.getEmailet().get(pos).setAdresa(vlera.getText().toString());
                    emailAdapter.notifyItemChanged(pos);
                    emailAdapter.notifyDataSetChanged();

                }else{
                    Toast.makeText(mContext, "Fill the form!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alert.show();

    }

    private void editIm(final int pos) {

        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

        alert.setTitle("Edit IM");

        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popView = inflater.inflate(R.layout.dialog_emer_vlere, null);

        final EditText emri = (EditText)popView.findViewById(R.id.emri);
        final EditText vlera= (EditText)popView.findViewById(R.id.vlera);

        final TextView emri_txt = (TextView)popView.findViewById(R.id.emri_txt);
        final TextView vlera_txt = (TextView)popView.findViewById(R.id.vlera_txt);

        emri_txt.setText("Name");
        emri.setHint("Work/Persona/Other");
        vlera_txt.setText("Email");
        vlera.setHint("Email address");

        emri.setText(kontakt.getMessengeret().get(pos).getEmri());
        vlera.setText(kontakt.getMessengeret().get(pos).getAdresa());

        alert.setView(popView);
        alert.setCancelable(true);
        alert.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(emri.getText().length()>0 && vlera.getText().length()>0){
                    kontakt.getMessengeret().get(pos).setEmri(emri.getText().toString());
                    kontakt.getMessengeret().get(pos).setAdresa(vlera.getText().toString());
                    imAdapter.notifyItemChanged(pos);
                    imAdapter.notifyDataSetChanged();

                }else{
                    Toast.makeText(mContext, "Fill the form!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alert.show();

    }

    private void editAddress(final int pos) {

        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

        alert.setTitle("Edit Address");

        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popView = inflater.inflate(R.layout.dialog_address, null);

        final EditText poBox = (EditText)popView.findViewById(R.id.poBox);
        final EditText street= (EditText)popView.findViewById(R.id.street);
        final EditText city= (EditText)popView.findViewById(R.id.city);
        final EditText state= (EditText)popView.findViewById(R.id.state);
        final EditText postalCode= (EditText)popView.findViewById(R.id.postalCode);
        final EditText country= (EditText)popView.findViewById(R.id.country);
        final EditText type= (EditText)popView.findViewById(R.id.type);

        poBox.setText(kontakt.getAdresat().get(pos).getPoBox());
        street.setText(kontakt.getAdresat().get(pos).getStreet());
        city.setText(kontakt.getAdresat().get(pos).getCity());
        state.setText(kontakt.getAdresat().get(pos).getState());
        postalCode.setText(kontakt.getAdresat().get(pos).getPostalCode());
        country.setText(kontakt.getAdresat().get(pos).getCountry());
        type.setText(kontakt.getAdresat().get(pos).getType());

        alert.setView(popView);
        alert.setCancelable(true);
        alert.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(street.getText().length()>0 && city.getText().length()>0){

                    kontakt.getAdresat().get(pos).setPoBox(poBox.getText().toString());
                    kontakt.getAdresat().get(pos).setStreet(street.getText().toString());
                    kontakt.getAdresat().get(pos).setCity(city.getText().toString());
                    kontakt.getAdresat().get(pos).setState(state.getText().toString());
                    kontakt.getAdresat().get(pos).setPostalCode(postalCode.getText().toString());
                    kontakt.getAdresat().get(pos).setCountry(country.getText().toString());
                    kontakt.getAdresat().get(pos).setType(type.getText().toString());

                    addressAdapter.notifyItemChanged(pos);
                    addressAdapter.notifyDataSetChanged();

                }else{
                    Toast.makeText(mContext, "Fill the form!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alert.show();

    }

    private void newElement(){
        if(currentTab==1){
            //new phone nr
            newPhoneNr();
        }else if(currentTab==2){
            //new email
            newEmail();
        }else if(currentTab==3){
            //new address
            newAddress();
        }else if(currentTab==4){
            //new im
            newIm();
        }
    }

    private void newPhoneNr(){

        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

        alert.setTitle("New Phone Number");

        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popView = inflater.inflate(R.layout.dialog_emer_vlere, null);

        final EditText emri = (EditText)popView.findViewById(R.id.emri);
        final EditText vlera= (EditText)popView.findViewById(R.id.vlera);

        alert.setView(popView);
        alert.setCancelable(true);
        alert.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(emri.getText().length()>0 && vlera.getText().length()>0){
                    KontaktTelefoni newElem = new KontaktTelefoni();

                    newElem.setEmri(emri.getText().toString());
                    newElem.setNumri(vlera.getText().toString());

                    if(kontakt.getTelefonat()==null) kontakt.setTelefonat(new ArrayList<KontaktTelefoni>());
                    kontakt.getTelefonat().add(newElem);

                    phoneAdapter.notifyDataSetChanged();

                }else{
                    Toast.makeText(mContext, "Fill the form!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    private void newEmail(){
        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

        alert.setTitle("New Email");

        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popView = inflater.inflate(R.layout.dialog_emer_vlere, null);

        final EditText emri = (EditText)popView.findViewById(R.id.emri);
        final EditText vlera= (EditText)popView.findViewById(R.id.vlera);

        final TextView emri_txt = (TextView)popView.findViewById(R.id.emri_txt);
        final TextView vlera_txt = (TextView)popView.findViewById(R.id.vlera_txt);

        emri_txt.setText("Name");
        emri.setHint("Work/Persona/Other");
        vlera_txt.setText("Email");
        vlera.setHint("Email address");

        alert.setView(popView);
        alert.setCancelable(true);
        alert.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(emri.getText().length()>0 && vlera.getText().length()>0){
                    KontaktEmail newElem = new KontaktEmail();

                    newElem.setEmri(emri.getText().toString());
                    newElem.setAdresa(vlera.getText().toString());

                    if(kontakt.getEmailet() ==null) kontakt.setEmailet(new ArrayList<KontaktEmail>());
                    kontakt.getEmailet().add(newElem);

                    emailAdapter.notifyDataSetChanged();

                }else{
                    Toast.makeText(mContext, "Fill the form!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    private void newAddress(){
        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

        alert.setTitle("New Address");

        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popView = inflater.inflate(R.layout.dialog_address, null);

        final EditText poBox = (EditText)popView.findViewById(R.id.poBox);
        final EditText street= (EditText)popView.findViewById(R.id.street);
        final EditText city= (EditText)popView.findViewById(R.id.city);
        final EditText state= (EditText)popView.findViewById(R.id.state);
        final EditText postalCode= (EditText)popView.findViewById(R.id.postalCode);
        final EditText country= (EditText)popView.findViewById(R.id.country);
        final EditText type= (EditText)popView.findViewById(R.id.type);


        alert.setView(popView);
        alert.setCancelable(true);
        alert.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(street.getText().length()>0 && city.getText().length()>0){
                    KontaktAdresa newElem = new KontaktAdresa();

                    newElem.setPoBox(poBox.getText().toString());
                    newElem.setStreet(street.getText().toString());
                    newElem.setCity(city.getText().toString());
                    newElem.setState(state.getText().toString());
                    newElem.setPostalCode(postalCode.getText().toString());
                    newElem.setCountry(country.getText().toString());
                    newElem.setType(type.getText().toString());

                    if(kontakt.getAdresat()==null) kontakt.setAdresat(new ArrayList<KontaktAdresa>());
                    kontakt.getAdresat().add(newElem);

                    addressAdapter.notifyDataSetChanged();

                }else{
                    Toast.makeText(mContext, "Fill the form!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    private void newIm(){
        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

        alert.setTitle("New IM");

        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popView = inflater.inflate(R.layout.dialog_emer_vlere, null);

        final EditText emri = (EditText)popView.findViewById(R.id.emri);
        final EditText vlera= (EditText)popView.findViewById(R.id.vlera);

        final TextView emri_txt = (TextView)popView.findViewById(R.id.emri_txt);
        final TextView vlera_txt = (TextView)popView.findViewById(R.id.vlera_txt);

        emri_txt.setText("Name");
        emri.setHint("Gtalk/Skype/Facebook");
        vlera_txt.setText("Nickname");
        vlera.setHint("IM nickname");

        alert.setView(popView);
        alert.setCancelable(true);
        alert.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(emri.getText().length()>0 && vlera.getText().length()>0){
                    KontaktMessenger newElem = new KontaktMessenger();

                    newElem.setEmri(emri.getText().toString());
                    newElem.setAdresa(vlera.getText().toString());

                    if(kontakt.getMessengeret() ==null) kontakt.setMessengeret(new ArrayList<KontaktMessenger>());
                    kontakt.getMessengeret().add(newElem);

                    imAdapter.notifyDataSetChanged();

                }else{
                    Toast.makeText(mContext, "Fill the form!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    private class TabKlik implements View.OnClickListener{
        private int pos;
        public TabKlik(int pos){
            this.pos=pos;
        }

        @Override
        public void onClick(View view) {

            currentTab  = pos;

            if(pos == 1){
                recyclerView.setAdapter(phoneAdapter);
                new_element.setText("Create new phone number");

                tab1.setBackgroundColor(mContext.getResources().getColor(R.color.primary));
                tab1.setTextColor(mContext.getResources().getColor(R.color.bardhe));

                tab2.setTextColor(mContext.getResources().getColor(R.color.primary));
                tab2.setBackgroundColor(mContext.getResources().getColor(R.color.bardhe));

                tab3.setTextColor(mContext.getResources().getColor(R.color.primary));
                tab3.setBackgroundColor(mContext.getResources().getColor(R.color.bardhe));

                tab4.setTextColor(mContext.getResources().getColor(R.color.primary));
                tab4.setBackgroundColor(mContext.getResources().getColor(R.color.bardhe));

            }else if(pos == 2){
                recyclerView.setAdapter(emailAdapter);
                new_element.setText("Create new email address");

                tab2.setBackgroundColor(mContext.getResources().getColor(R.color.primary));
                tab2.setTextColor(mContext.getResources().getColor(R.color.bardhe));

                tab1.setTextColor(mContext.getResources().getColor(R.color.primary));
                tab1.setBackgroundColor(mContext.getResources().getColor(R.color.bardhe));

                tab3.setTextColor(mContext.getResources().getColor(R.color.primary));
                tab3.setBackgroundColor(mContext.getResources().getColor(R.color.bardhe));

                tab4.setTextColor(mContext.getResources().getColor(R.color.primary));
                tab4.setBackgroundColor(mContext.getResources().getColor(R.color.bardhe));


            }else if(pos == 3){
                recyclerView.setAdapter(addressAdapter);
                new_element.setText("Create new address");

                tab3.setBackgroundColor(mContext.getResources().getColor(R.color.primary));
                tab3.setTextColor(mContext.getResources().getColor(R.color.bardhe));

                tab2.setTextColor(mContext.getResources().getColor(R.color.primary));
                tab2.setBackgroundColor(mContext.getResources().getColor(R.color.bardhe));

                tab1.setTextColor(mContext.getResources().getColor(R.color.primary));
                tab1.setBackgroundColor(mContext.getResources().getColor(R.color.bardhe));

                tab4.setTextColor(mContext.getResources().getColor(R.color.primary));
                tab4.setBackgroundColor(mContext.getResources().getColor(R.color.bardhe));


            }else if(pos == 4){
                recyclerView.setAdapter(imAdapter);
                new_element.setText("Create new IM");

                tab4.setBackgroundColor(mContext.getResources().getColor(R.color.primary));
                tab4.setTextColor(mContext.getResources().getColor(R.color.bardhe));

                tab2.setTextColor(mContext.getResources().getColor(R.color.primary));
                tab2.setBackgroundColor(mContext.getResources().getColor(R.color.bardhe));

                tab3.setTextColor(mContext.getResources().getColor(R.color.primary));
                tab3.setBackgroundColor(mContext.getResources().getColor(R.color.bardhe));

                tab1.setTextColor(mContext.getResources().getColor(R.color.primary));
                tab1.setBackgroundColor(mContext.getResources().getColor(R.color.bardhe));
            }

        }
    };

    public interface OnDialogEditEvent{
        public void onDeleteKontakt(Kontakt kontakt);
        public void onEditKontakt(Kontakt kontakt);
    }


}
