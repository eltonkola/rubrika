package com.aldroid.rubrika;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.aldroid.rubrika.adapter.ContactsAdapter;
import com.aldroid.rubrika.model.EksportTemplate;
import com.aldroid.rubrika.model.EksportTipi;
import com.aldroid.rubrika.model.Kontakt;
import com.aldroid.rubrika.task.KontaktTaskEvents;
import com.aldroid.rubrika.task.LoadKontaktet;
import com.aldroid.rubrika.task.RenderTaskEvents;
import com.aldroid.rubrika.task.RubrikaToCsv;
import com.aldroid.rubrika.task.RubrikaToExl;
import com.aldroid.rubrika.task.RubrikaToPdf;
import com.aldroid.rubrika.utils.Utils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class KrijoActivity extends ActionBarActivity{

    public static EksportTemplate template;
    private ContactsAdapter mAdapter; // The main query adapter

    private ArrayList<Kontakt> kontaktet = new ArrayList<Kontakt>();

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_krijo);


        getSupportActionBar().setDisplayUseLogoEnabled(true);

        getSupportActionBar().setTitle(getString(R.string.main_create));
        getSupportActionBar().setSubtitle(getString(R.string.title_select_contacts));
        getSupportActionBar().setLogo(R.drawable.logo);
        getSupportActionBar().setIcon(R.drawable.logo);

        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button button_krijo = (Button)findViewById(R.id.button_krijo);

        // 1. get a reference to recyclerView
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);

        // 2. set layoutManger
        LinearLayoutManager layoutManager = new LinearLayoutManager(KrijoActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.scrollToPosition(0);
        recyclerView.setLayoutManager(layoutManager);



        // 3. create an adapter
        mAdapter = new ContactsAdapter(kontaktet);

        mAdapter.setOnItemClickListener(new ContactsAdapter.OnItemClickListenerVk() {

            @Override
            public void onItemKlick(View view, int position) {

                Kontakt kontakt = kontaktet.get(position);
                UserEditDialog dialogEdit = new UserEditDialog(KrijoActivity.this, kontakt, new UserEditDialog.OnDialogEditEvent() {
                    @Override
                    public void onDeleteKontakt(Kontakt kontakt) {
                        Utils.log("onDeleteKontakt: " + kontakt);
                        int position = kontaktet.indexOf(kontakt);
                        kontaktet.remove(kontakt);
                        mAdapter.notifyItemRemoved(position);
//                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onEditKontakt(Kontakt kontakt) {
                        Utils.log("onEditKontakt returned : " + kontakt);
                        int position = kontaktet.indexOf(kontakt);
                        kontaktet.set(position, kontakt);
                        mAdapter.notifyItemChanged(position);
                        mAdapter.notifyDataSetChanged();

//                        Toast.makeText(KrijoActivity.this, "Updated position:" + position , Toast.LENGTH_SHORT).show();


                    }
                });
                dialogEdit.show();

//                Toast.makeText(KrijoActivity.this,"TODO - edit:" + kontakt.getEmri() , Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onItemDelete(int position) {
                kontaktet.remove(position);
                mAdapter.notifyItemRemoved(position);
            }
        });

        // 4. set adapter
        recyclerView.setAdapter(mAdapter);
        // 5. set item animator to DefaultAnimator
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        button_krijo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(kontaktet.size()>0) {
                    String fName = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(new Date());
                    String ext = "";
                    if(template.getTipi() == EksportTipi.PDF_SIMPLE || template.getTipi() == EksportTipi.PDF_COMPLETE){
                        ext=".pdf";
                    }else if(template.getTipi() == EksportTipi.CSV_SIMPLE || template.getTipi() == EksportTipi.CSV_COMPLETE){
                        ext=".csv";
                    }else{
                        ext=".xls";
                    }

                    File dir = new File(Utils.DOWN_URL);
                    if(!dir.exists()) dir.mkdirs();
                    String p = Utils.DOWN_URL + fName + ext;

                    RenderTaskEvents renderEvents = new RenderTaskEvents() {
                        @Override
                        public void error() {
                            Toast.makeText(KrijoActivity.this, getString(R.string.error_exporting_contacts), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void krijuar(String path) {
                           postCreation(path);
                        }

                        @Override
                        public void progressHap() {
                            progressShow(getString(R.string.creating_txt),getString(R.string.creating_tit),true);
                        }

                        @Override
                        public void progressMbyll() {
                            progressCancel();
                        }

                        @Override
                        public void progress(Integer val, Integer tot) {
                            progressProgress(val,tot);
                        }
                    };
                    if(template.getTipi() == EksportTipi.PDF_SIMPLE || template.getTipi() == EksportTipi.PDF_COMPLETE) {
                        Utils.execute(new RubrikaToPdf(KrijoActivity.this, p, kontaktet, template, renderEvents));
                    }else if(template.getTipi() == EksportTipi.XLS_SIMPLE || template.getTipi() == EksportTipi.XLS_COMPLETE){
                        Utils.execute(new RubrikaToExl(KrijoActivity.this, p, kontaktet, template, renderEvents));
                    }else{
                        Utils.execute(new RubrikaToCsv(KrijoActivity.this, p, kontaktet, template, renderEvents));
                    }
                }else{
                    Toast.makeText(KrijoActivity.this,getString(R.string.error_exporting_nocontacts), Toast.LENGTH_SHORT).show();
                }
            }
        });


        new LoadKontaktet(KrijoActivity.this, template.isAllDetails() ,new KontaktTaskEvents() {
            @Override
            public void error() {
                Toast.makeText(KrijoActivity.this,getString(R.string.error_loading_contacts), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void karikim(ArrayList<Kontakt> kontaktet) {
                KrijoActivity.this.kontaktet.addAll(kontaktet);
                mAdapter.notifyItemInserted(0);
            }

            @Override
            public void progressHap() {
                progressShow(getString(R.string.loading_txt), getString(R.string.loading_tit), true);
            }

            @Override
            public void progressMbyll() {
                progressCancel();
            }

            @Override
            public void progress(Integer val, Integer tot) {
                progressProgress(val, tot);
            }
        }).execute();


        // Create the adView
        if(Utils.showAds) {
            AdView mAdView = (AdView)findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }else{
            findViewById(R.id.adView).setVisibility(View.GONE);
        }


    }

    private void postCreation(String file){

        final File elem = new File(file);

        final CharSequence[] items = {
                                        getString(R.string.result_menu_open),
                                        getString(R.string.result_menu_delete),
                                        getString(R.string.result_menu_print),
                                        getString(R.string.result_menu_share),
                                        getString(R.string.result_menu_close)
                                    };

        AlertDialog.Builder builder = new AlertDialog.Builder(KrijoActivity.this);
        builder.setTitle(getString(R.string.result_menu_tit));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if(item==0){
                    dialog.dismiss();
                    finish();
                    previewFile(elem);
                }else if(item==1){
                    Utils.execute(new DeleteFIle(elem));
                    dialog.dismiss();
                    Toast.makeText(KrijoActivity.this, getString(R.string.op_file_deleted), Toast.LENGTH_SHORT).show();
                }else if(item==2){
                    try{
                        Uri uri = Uri.fromFile(elem);
                        Intent intent = new Intent ("org.androidprinting.intent.action.PRINT");
                        intent.setDataAndType( uri, "text/plain" );
                        startActivityForResult(intent, 0);
                    }catch (Exception e){
                        Toast.makeText(KrijoActivity.this, getString(R.string.op_no_print), Toast.LENGTH_SHORT).show();
                    }
                    dialog.dismiss();
                    finish();
                }else if(item==3){
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("file/*");
                    share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(elem));
                    startActivity(Intent.createChooser(share, getString(R.string.op_share_with)));
                    dialog.dismiss();
                    finish();
                }else{
                    dialog.dismiss();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();


    }

    private class DeleteFIle extends AsyncTask<Void,Void,Boolean> {

        private File f;

        public DeleteFIle(File path){
            f = path;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                f.delete();
                return true;
            }catch (Exception e){
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean o) {
            Toast.makeText(KrijoActivity.this, getString(R.string.op_file_deleted), Toast.LENGTH_SHORT).show();
            super.onPostExecute(o);
        }

    };

    private void previewFile(File file) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            if(file.getName().endsWith(".pdf")) {
                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
            }else  if(file.getName().endsWith(".xls")){
                intent.setDataAndType(Uri.fromFile(file), "application/vnd.ms-excel");
            }else{
                intent.setDataAndType(Uri.fromFile(file), "text/csv");
            }
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }catch (Exception e){
            Toast.makeText(KrijoActivity.this, getString(R.string.op_app_preview), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_krijo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_fshij_pa_foto) {
            //fshij tere ata pa foto
            ArrayList<Kontakt> kontaktetTmp = new ArrayList<Kontakt>();
            kontaktetTmp.addAll(kontaktet);

            int firstIndexRemoved = 0;
            int k = 0;
            for(Kontakt kon:kontaktetTmp){

                if(kon.getThumbnail_uri()==null){
                    Utils.log("remove:" + kon.getEmri());
                    kontaktet.remove(kon);
                    if(firstIndexRemoved==0)firstIndexRemoved = k;
                }else{
                    Utils.log("ok "+ kon.getEmri() + ":"+ kon.getThumbnail_uri());
                }

                k++;
            }
            mAdapter.notifyDataSetChanged();
            item.setEnabled(false);

            return true;
        }else if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }



    private ProgressDialog progressDialog;
    public void progressShow(String msg, String tit, boolean progress) {

        try {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(msg);
            progressDialog.setTitle(tit);

            if(progress)progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

            progressDialog.setCancelable(false);
            progressDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void progressCancel() {
        try {
            progressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void progressProgress(Integer val, Integer tot) {
        if(progressDialog.getMax()!=tot.intValue()){
            progressDialog.setMax(tot);
        }
        progressDialog.setProgress(val);
    }

}
