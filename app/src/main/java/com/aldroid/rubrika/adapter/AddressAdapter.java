package com.aldroid.rubrika.adapter;

import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import com.aldroid.rubrika.R;
import com.aldroid.rubrika.model.Kontakt;
import com.aldroid.rubrika.model.KontaktAdresa;
import com.aldroid.rubrika.model.KontaktEmail;
import com.aldroid.rubrika.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

    private List<KontaktAdresa> itemsData;
    OnItemClickListenerVk mItemClickListener;

    public AddressAdapter(List<KontaktAdresa> itemsData) {
        if(itemsData!=null) {
            this.itemsData = itemsData;
        }else{
            this.itemsData= new ArrayList<KontaktAdresa>();
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AddressAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_emri_vlera, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
        }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        final KontaktAdresa ad = itemsData.get(position);

        holder.emri.setText(ad.getType());
        holder.ikona.setImageResource(R.drawable.explore);

        String adrx = "";
        if(ad.getCountry()!=null && ad.getState()!=null && ad.getCity()!=null && ad.getStreet()!=null && ad.getPoBox()!=null && ad.getPostalCode()!=null){
            adrx += " " + ad.getCountry() + " " + ad.getState()+ " " + ad.getCity() + " " + ad.getStreet() + " " + ad.getPoBox() + " " + ad.getPostalCode() ;
        }else if(ad.getState()!=null && ad.getCity()!=null && ad.getStreet()!=null && ad.getPoBox()!=null && ad.getPostalCode()!=null){
            adrx += " " + ad.getState()+ " " + ad.getCity() + " " + ad.getStreet() + " " + ad.getPoBox() + " " + ad.getPostalCode();
        }else if(ad.getCity()!=null && ad.getStreet()!=null && ad.getPoBox()!=null && ad.getPostalCode()!=null){
            adrx += " " + ad.getCity() + " " + ad.getStreet() + " " + ad.getPoBox() + " " + ad.getPostalCode() ;
        }else if(ad.getStreet()!=null && ad.getPoBox()!=null && ad.getPostalCode()!=null){
            adrx += " " + ad.getStreet() + " " + ad.getPoBox() + " " + ad.getPostalCode();
        }
        else{
            Utils.log("no address found for:" + ad.getType());
            adrx = "No address";
        }

        holder.vlera.setText(adrx);


    }

// inner class to hold a reference to each item of RecyclerView
public class ViewHolder extends RecyclerView.ViewHolder {

    public TextView emri;
    public TextView vlera;
    public ImageView ikona;
    public ImageView button;

    public ViewHolder(View itemLayoutView) {
        super(itemLayoutView);
        emri = (TextView)itemLayoutView.findViewById(R.id.emri);
        vlera = (TextView)itemLayoutView.findViewById(R.id.vlera);
        ikona = (ImageView)itemLayoutView.findViewById(R.id.ikona);
        button = (ImageView)itemLayoutView.findViewById(R.id.button);

        itemLayoutView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemKlick(v, getPosition());
                }
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemDelete(getPosition());
                }
            }
        });
    }


}

    public interface OnItemClickListenerVk {
        public void onItemKlick(View view, int position);
        public void onItemDelete(int position);
    }

    public void setOnItemClickListener(final OnItemClickListenerVk mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.size();
    }

}
