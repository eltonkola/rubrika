package com.aldroid.rubrika.adapter;

/**
 * Created by Elton on 07/09/2014.
 */

import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aldroid.rubrika.R;
import com.aldroid.rubrika.model.EksportTemplate;

public class TemplateAdapter extends RecyclerView.Adapter<TemplateAdapter.ViewHolder> {
    private EksportTemplate[] itemsData;

    OnItemClickListenerVk mItemClickListener;


    public TemplateAdapter(EksportTemplate[] itemsData) {
        this.itemsData = itemsData;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TemplateAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_template, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData


        if(itemsData[position].isDjathtas()){
            viewHolder.emri.setGravity(Gravity.RIGHT);
            viewHolder.pershkrimi1.setGravity(Gravity.RIGHT);
            viewHolder.pershkrimi2.setGravity(Gravity.RIGHT);
        }else{
            viewHolder.emri.setGravity(Gravity.LEFT);
            viewHolder.pershkrimi1.setGravity(Gravity.LEFT);
            viewHolder.pershkrimi2.setGravity(Gravity.LEFT);
        }


        viewHolder.emri.setText(itemsData[position].getEmri() );
        viewHolder.pershkrimi1.setText(itemsData[position].getPershkrimi1());
        viewHolder.pershkrimi2.setText(itemsData[position].getPershkrimi2());
        viewHolder.imazhi.setImageResource(itemsData[position].getImazhi());


    }

    // inner class to hold a reference to each item of RecyclerView
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView emri, pershkrimi1, pershkrimi2;
        public ImageView imazhi;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            emri = (TextView) itemLayoutView.findViewById(R.id.emri);
            pershkrimi1= (TextView) itemLayoutView.findViewById(R.id.pershkrimi1);
            pershkrimi2= (TextView) itemLayoutView.findViewById(R.id.pershkrimi2);
            imazhi = (ImageView) itemLayoutView.findViewById(R.id.imazhi);
            itemLayoutView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }

    }

    public interface OnItemClickListenerVk {
        public void onItemClick(View view , int position);

    }

    public void setOnItemClickListener(final OnItemClickListenerVk mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.length;
    }
}