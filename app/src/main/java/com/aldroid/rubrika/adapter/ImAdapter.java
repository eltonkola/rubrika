package com.aldroid.rubrika.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aldroid.rubrika.R;
import com.aldroid.rubrika.model.Kontakt;
import com.aldroid.rubrika.model.KontaktEmail;
import com.aldroid.rubrika.model.KontaktMessenger;

import java.util.ArrayList;
import java.util.List;

public class ImAdapter extends RecyclerView.Adapter<ImAdapter.ViewHolder> {

    private List<KontaktMessenger> itemsData;
    OnItemClickListenerVk mItemClickListener;


    public ImAdapter(List<KontaktMessenger> itemsData) {

        if(itemsData!=null) {
            this.itemsData = itemsData;
        }else{
            this.itemsData= new ArrayList<KontaktMessenger>();
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ImAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_emri_vlera, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
        }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final KontaktMessenger kontakt = itemsData.get(position);
        holder.emri.setText(kontakt.getEmri());
        holder.vlera.setText(kontakt.getAdresa());
        holder.ikona.setImageResource(R.drawable.message);
    }

// inner class to hold a reference to each item of RecyclerView
public class ViewHolder extends RecyclerView.ViewHolder {

    public TextView emri;
    public TextView vlera;
    public ImageView button;
    public ImageView ikona;

    public ViewHolder(View itemLayoutView) {
        super(itemLayoutView);
        emri = (TextView)itemLayoutView.findViewById(R.id.emri);
        vlera = (TextView)itemLayoutView.findViewById(R.id.vlera);
        button = (ImageView)itemLayoutView.findViewById(R.id.button);
        ikona = (ImageView)itemLayoutView.findViewById(R.id.ikona);

        itemLayoutView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemKlick(v, getPosition());
                }
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemDelete(getPosition());
                }
            }
        });
    }


}

    public interface OnItemClickListenerVk {
        public void onItemKlick(View view, int position);
        public void onItemDelete(int position);
    }

    public void setOnItemClickListener(final OnItemClickListenerVk mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.size();
    }

}
