package com.aldroid.rubrika.adapter;

import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import com.aldroid.rubrika.R;
import com.aldroid.rubrika.model.Kontakt;
import com.aldroid.rubrika.model.KontaktTelefoni;
import com.aldroid.rubrika.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class PhoneAdapter extends RecyclerView.Adapter<PhoneAdapter.ViewHolder> {

    private List<KontaktTelefoni> itemsData;
    OnItemClickListenerVk mItemClickListener;


    public PhoneAdapter(List<KontaktTelefoni> itemsData) {

        if(itemsData!=null) {
            this.itemsData = itemsData;
        }else{
            this.itemsData= new ArrayList<KontaktTelefoni>();
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PhoneAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_emri_vlera, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
        }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final KontaktTelefoni kontakt = itemsData.get(position);
        holder.emri.setText(kontakt.getEmri());
        holder.vlera.setText(kontakt.getNumri()+"");
        holder.ikona.setImageResource(R.drawable.call);
    }

// inner class to hold a reference to each item of RecyclerView
public class ViewHolder extends RecyclerView.ViewHolder {

    public TextView emri;
    public TextView vlera;
    public ImageView button;
    public ImageView ikona;

    public ViewHolder(View itemLayoutView) {
        super(itemLayoutView);
        emri = (TextView)itemLayoutView.findViewById(R.id.emri);
        vlera = (TextView)itemLayoutView.findViewById(R.id.vlera);
        button = (ImageView)itemLayoutView.findViewById(R.id.button);
        ikona = (ImageView)itemLayoutView.findViewById(R.id.ikona);

        itemLayoutView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemKlick(v, getPosition());
                }
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemDelete(getPosition());
                }
            }
        });
    }


}

    public interface OnItemClickListenerVk {
        public void onItemKlick(View view, int position);
        public void onItemDelete(int position);
    }

    public void setOnItemClickListener(final OnItemClickListenerVk mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.size();
    }

}
