package com.aldroid.rubrika.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aldroid.rubrika.R;
import com.aldroid.rubrika.utils.FileSizeFormatter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class ArchiveAdapter extends RecyclerView.Adapter<ArchiveAdapter.ViewHolder>{

    private ArrayList<File> itemsData;

    OnItemClickListenerVk mItemClickListener;

    public ArchiveAdapter(ArrayList<File> itemsData) {
        this.itemsData = itemsData;
    }
    // Create new views (invoked by the layout manager)
    @Override
    public ArchiveAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_archive, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        File f = itemsData.get(position);
        viewHolder.txt1.setText(f.getName());
        viewHolder.txt2.setText("Size: " + FileSizeFormatter.formatFileSize(f.length()) );
        Date lastModDate = new Date(f.lastModified());
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        viewHolder.txt3.setText(dateFormatter.format(lastModDate));
        if(f.getName().endsWith(".pdf")){
            viewHolder.avatar.setImageResource(R.drawable.ic_pdf);
        }else if(f.getName().endsWith(".xls")){
            viewHolder.avatar.setImageResource(R.drawable.ic_xls);
        }else{
            viewHolder.avatar.setImageResource(R.drawable.ic_csv);
        }
    }

    // inner class to hold a reference to each item of RecyclerView
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txt1, txt2, txt3;
        public ImageView avatar;
        private Button more;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txt1 = (TextView) itemLayoutView.findViewById(com.aldroid.rubrika.R.id.txt1);
            txt2= (TextView) itemLayoutView.findViewById(com.aldroid.rubrika.R.id.txt2);
            txt3= (TextView) itemLayoutView.findViewById(com.aldroid.rubrika.R.id.txt3);
            avatar = (ImageView) itemLayoutView.findViewById(com.aldroid.rubrika.R.id.avatar);
            more = (Button) itemLayoutView.findViewById(com.aldroid.rubrika.R.id.more);


            more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mItemClickListener != null) {
                        mItemClickListener.onMoreClick(more, getPosition());
                    }
                }
            });

            itemLayoutView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }

    }

    public interface OnItemClickListenerVk {
        public void onItemClick(View view , int position);
        public void onMoreClick(View view , int position);

    }

    public void setOnItemClickListener(final OnItemClickListenerVk mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.size();
    }
}
