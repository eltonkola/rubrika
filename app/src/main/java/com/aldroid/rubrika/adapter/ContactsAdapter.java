package com.aldroid.rubrika.adapter;

import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import com.aldroid.rubrika.R;
import com.aldroid.rubrika.model.Kontakt;
import com.aldroid.rubrika.utils.Utils;

import java.util.List;

public class ContactsAdapter  extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    private List<Kontakt> itemsData;
    OnItemClickListenerVk mItemClickListener;


    public ContactsAdapter(List<Kontakt> itemsData) {
        this.itemsData = itemsData;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ContactsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_kontakt, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
        }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        final Kontakt kontakt = itemsData.get(position);

        holder.emri.setText(kontakt.getEmri());

        if(kontakt.getEmailet().size()>0){
            holder.email.setText(kontakt.getEmailet().get(0).getEmri() + ":" + kontakt.getEmailet().get(0).getAdresa());
        }else{
            holder.email.setText("..no email..");
        }

        if(kontakt.getTelefonat().size()>0){
            holder.telefoni.setText(kontakt.getTelefonat().get(0).getEmri() + ":" + kontakt.getTelefonat().get(0).getNumri());
        }else{
            holder.telefoni.setText("..no phone nr..");
        }

        final Uri contactUri = ContactsContract.Contacts.getLookupUri(Long.parseLong(kontakt.getId()) ,kontakt.getLookup_key());
        holder.ikona.assignContactUri(contactUri);

        String photoUri = kontakt.getThumbnail_uri();

        Utils.log("-------------------------");
        Utils.log("getThumbnail_uri:" + photoUri);
        Utils.log("getLookup_key:" + kontakt.getLookup_key());
        Utils.log("getId:" + kontakt.getId());

//        final  QuickContactBadge ikona = holder.ikona;

        if(photoUri!=null) {
            Utils.log("contact foto:" + photoUri);
            holder.ikona.setImageURI(Uri.parse(photoUri));
        }else{
            holder.ikona.setImageResource(R.drawable.ic_launcher);
        }





    }

// inner class to hold a reference to each item of RecyclerView
public class ViewHolder extends RecyclerView.ViewHolder {

    public TextView emri;
    public TextView email;
    public TextView telefoni;
    public QuickContactBadge ikona;
    public ImageView button;

    public ViewHolder(View itemLayoutView) {
        super(itemLayoutView);
        emri = (TextView)itemLayoutView.findViewById(R.id.emri);
        email = (TextView)itemLayoutView.findViewById(R.id.email);
        telefoni = (TextView)itemLayoutView.findViewById(R.id.telefoni);
        ikona = (QuickContactBadge)itemLayoutView.findViewById(R.id.ikona);
        button = (ImageView)itemLayoutView.findViewById(R.id.button);

        itemLayoutView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemKlick(v, getPosition());
                }
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemDelete(getPosition());
                }
            }
        });
    }


}

    public interface OnItemClickListenerVk {
        public void onItemKlick(View view , int position);
        public void onItemDelete(int position);
    }

    public void setOnItemClickListener(final OnItemClickListenerVk mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.size();
    }

}
