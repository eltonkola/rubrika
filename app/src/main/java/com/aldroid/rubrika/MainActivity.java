package com.aldroid.rubrika;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.aldroid.rubrika.adapter.TitleNavigationAdapter;
import com.aldroid.rubrika.utils.Utils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity implements ActionBar.TabListener{

    private TitleNavigationAdapter adapter;
    private ArrayList<SpinnerNavItem> navSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setLogo(R.drawable.logo);
        actionBar.setIcon(R.drawable.logo);

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        ActionBar.Tab tab1 = actionBar.newTab().setText(getString(R.string.main_create));
        ActionBar.Tab tab2 = actionBar.newTab().setText(getString(R.string.main_archive));
        tab1.setTabListener(this);
        tab2.setTabListener(this);
        actionBar.addTab(tab1);
        actionBar.addTab(tab2);

        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("Rubrika");


        // ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
        adapterFragment = new RubrikaFragmentAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager)findViewById(R.id.pager);
        mViewPager.setAdapter(adapterFragment);

        mViewPager.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        // When swiping between pages, select the
                        // corresponding tab.
                        getSupportActionBar().setSelectedNavigationItem(position);
                    }
                });


        if(Utils.showAds) {
            AdView mAdView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }else{
            findViewById(R.id.adView).setVisibility(View.GONE);
        }


    }

    // Since this is an object collection, use a FragmentStatePagerAdapter,
// and NOT a FragmentPagerAdapter.
    public class RubrikaFragmentAdapter extends FragmentStatePagerAdapter{
        public RubrikaFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int i) {
            if(i==0) {
                return NewFragment.newInstance();
            }else{
                return ArchiveFragment.newInstance();
            }
        }

        @Override
        public int getCount() {
            return 100;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "OBJECT " + (position + 1);
        }
    }

    private RubrikaFragmentAdapter adapterFragment;
    private ViewPager mViewPager;

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        if(mViewPager!=null)mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }



    public class SpinnerNavItem {
        private String title;
        private int icon;
        public SpinnerNavItem(String title, int icon){
            this.title = title;
            this.icon = icon;
        }
        public String getTitle(){
            return this.title;
        }
        public int getIcon(){
            return this.icon;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
       if (id == R.id.action_info) {
            Intent i = new Intent(MainActivity.this, WebviewActivity.class);
            i.putExtra("titulli", getString(R.string.about));
            i.putExtra("url", "file:///android_asset/about.html");
            startActivity(i);

        }else if (id == R.id.action_more) {
            Uri makerm = Uri.parse("market://search?q=com.aldroid");
            Intent idx = new Intent(Intent.ACTION_VIEW, makerm);
            startActivity(idx);
        }else if (id == R.id.action_changelog) {
            final Dialog dialog = new Dialog(MainActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_about);
            dialog.findViewById(R.id.buttonClose).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.hide();
                }
            });
            dialog.show();
        }else if (id == R.id.action_contact) {
            final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            emailIntent.setType("plain/text");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,new String[] { "devaldroid@gmail.com" });
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,getString(R.string.mail_tit));
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,getString(R.string.mail_txt));
            startActivity(Intent.createChooser(emailIntent,getString(R.string.mail_send)));
        }else if (id == R.id.action_rate) {
            String myUrl = "market://details?id=com.aldroid.rubrika";
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(myUrl));
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

}
