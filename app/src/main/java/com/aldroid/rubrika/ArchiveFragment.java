package com.aldroid.rubrika;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.aldroid.rubrika.adapter.ArchiveAdapter;
import com.aldroid.rubrika.utils.Utils;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Elton on 06/09/2014.
 */
public class ArchiveFragment extends Fragment {

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ArchiveFragment newInstance() {
        ArchiveFragment fragment = new ArchiveFragment();
        return fragment;
    }

    public ArchiveFragment() {
    }

    private ArchiveAdapter mAdapter;
    private RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_archive, container, false);

        // 1. get a reference to recyclerView
        recyclerView = (RecyclerView)rootView.findViewById(R.id.recyclerView);

        // 2. set layoutManger
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // 5. set item animator to DefaultAnimator
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        kariko();

        return rootView;
    }


    private void previewFile(File file) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            if(file.getName().endsWith(".pdf")) {
                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
            }else  if(file.getName().endsWith(".xls")){
                intent.setDataAndType(Uri.fromFile(file), "application/vnd.ms-excel");
            }else{
                intent.setDataAndType(Uri.fromFile(file), "text/csv");
            }
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }catch (Exception e){
            Toast.makeText(getActivity(), getString(R.string.op_app_preview), Toast.LENGTH_SHORT).show();
        }
    }

    private ArrayList<File> itemsData = new ArrayList<File>();

    private void moreLong(View v, final File elem){

        Utils.log("onItemLongClick arg0:" + elem );
        final PopupMenu popupMenuSub = new PopupMenu(getActivity(), v);
        popupMenuSub.getMenu().add(Menu.NONE, 0, Menu.NONE, getString(R.string.result_menu_open));
        popupMenuSub.getMenu().add(Menu.NONE, 1, Menu.NONE, getString(R.string.result_menu_delete));
        popupMenuSub.getMenu().add(Menu.NONE, 2, Menu.NONE, getString(R.string.result_menu_print));
        popupMenuSub.getMenu().add(Menu.NONE, 3, Menu.NONE, getString(R.string.result_menu_share));

        popupMenuSub.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case 0:
                        previewFile(elem);
                        break;
                    case 1:
                        Utils.execute(new DeleteFIle(elem));
                        break;
                    case 2:
                        try{
                            Uri uri = Uri.fromFile(elem);
                            Intent intent = new Intent ("org.androidprinting.intent.action.PRINT");
                            if(elem.getName().endsWith(".pdf")) {
                                intent.setDataAndType(Uri.fromFile(elem), "application/pdf");
                            }else  if(elem.getName().endsWith(".xls")){
                                intent.setDataAndType(Uri.fromFile(elem), "application/vnd.ms-excel");
                            }else{
                                intent.setDataAndType(Uri.fromFile(elem), "text/comma-separated-values" ); //"text/csv"
                            }
                            getActivity().startActivityForResult(intent, 0);
                        }catch (Exception e){
                            Toast.makeText(getActivity(), getString(R.string.op_no_print), Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 3:
                        Intent share = new Intent(Intent.ACTION_SEND);
                        if(elem.getName().endsWith(".pdf")) {
                            share.setType("file/pdf");
                        }else{
                            share.setType("file/vnd.ms-excel");
                        }
                        share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(elem));
                        startActivity(Intent.createChooser(share, getString(R.string.op_share_with)));
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
        popupMenuSub.show();
    }

    private class DeleteFIle extends AsyncTask<Void,Void,Boolean> {

        private File f;

        public DeleteFIle(File path){
            f = path;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
               f.delete();
                return true;
            }catch (Exception e){
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean o) {
            Toast.makeText(getActivity(), getString(R.string.op_file_deleted), Toast.LENGTH_SHORT).show();
            kariko();
            super.onPostExecute(o);
        }

    };


    private void kariko(){
        itemsData = new ArrayList<File>();

        if(mAdapter!=null)mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(null);
        final String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File fileList = new File(Utils.DOWN_URL);
            if (fileList != null){
                File[] filenames = fileList.listFiles();

                if(filenames!=null && filenames.length>0){

                    for(File f: filenames){
                        if(!f.isDirectory() && (f.getName().endsWith(".pdf")  || f.getName().endsWith(".xls")  || f.getName().endsWith(".csv"))){
                            itemsData.add(f);
                            Utils.log("shto:" + f.getName());
                        }
                    }

                    // 3. create an adapter
                    mAdapter = new ArchiveAdapter(itemsData);

                    mAdapter.setOnItemClickListener(new ArchiveAdapter.OnItemClickListenerVk() {
                        @Override
                        public void onItemClick(View view, int position) {
                            previewFile(itemsData.get(position));
                        }

                        @Override
                        public void onMoreClick(View v,int position) {
                            moreLong(v,itemsData.get(position));
                        }
                    });
                    // 4. set adapter
                    recyclerView.setAdapter(mAdapter);
                }else{
                    Toast.makeText(getActivity(), getString(R.string.error_nofiles), Toast.LENGTH_SHORT).show();
                }

            }
        } else {
            Toast.makeText(getActivity(),getString(R.string.error_sdcard), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        kariko();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
}
