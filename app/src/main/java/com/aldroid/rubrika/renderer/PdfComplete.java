package com.aldroid.rubrika.renderer;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.aldroid.rubrika.model.Kontakt;
import com.aldroid.rubrika.model.KontaktAdresa;
import com.aldroid.rubrika.model.KontaktEmail;
import com.aldroid.rubrika.model.KontaktMessenger;
import com.aldroid.rubrika.model.KontaktTelefoni;
import com.aldroid.rubrika.task.RendererTaskEvents;
import com.aldroid.rubrika.utils.Utils;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Elton on 07/09/2014.
 */
public class PdfComplete{

    private ArrayList<Kontakt> kontaktet= new ArrayList<Kontakt>();
    private String filename;
    private Context context;
    private RendererTaskEvents progEvent;

    public void createPdf(String filename, ArrayList<Kontakt> kontaktet, Context context, RendererTaskEvents progEvent) throws IOException, DocumentException, Exception {
        this.filename=filename;
        this.kontaktet=kontaktet;
        this.progEvent = progEvent;
        this.context=context;


        //Setup a new PDF Document
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filename));
        writer.setPageEvent(new Watermark());
        document.setMargins(20, 20, 100, 20);
        document.open();
        document.newPage();

        //hapim template pdf nga assets
        AssetManager assManager = context.getAssets();

        try {

            PdfContentByte cb = writer.getDirectContent();
            //krijo kopertinen
            renderKopertina(document, writer);
            //krijo permbajtjen
            renderAll(document, writer);
            //faqja e fundit ka tere fotot
            document.newPage();
            renderFundit(document, writer);
            //fund
            document.close();



        } catch (IOException e) {
            e.printStackTrace();
            throw  new Exception();
        }

    }

    private byte[] getImg(String name){
        try {
            InputStream ims = context.getAssets().open(name);
            Bitmap bmp = BitmapFactory.decodeStream(ims);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            return stream.toByteArray();
        }catch (Exception e){
            //e.printStackTrace();
            return new byte[0];
        }

    }

    public byte[] getContactBitmapFromURI(Uri uri) {
        try {
            InputStream ims  = context.getContentResolver().openInputStream(uri);
            Bitmap bmp = BitmapFactory.decodeStream(ims);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            return stream.toByteArray();
        }catch (Exception e){
            return new byte[0];
        }
    }


    /**
     * Inner class to add a watermark to every page.
     */
    class Watermark extends PdfPageEventHelper {

        Font FONT = new Font(Font.FontFamily.HELVETICA, 52, Font.BOLD, new GrayColor(0.75f));

        public void onEndPage(PdfWriter writer, Document document) {
            //ColumnText.showTextAligned(writer.getDirectContentUnder(), com.itextpdf.text.Element.ALIGN_CENTER, new Phrase("my addres book", FONT), 297.5f, 421, writer.getPageNumber() % 2 == 1 ? 45 : -45);
            if(document.getPageNumber()>1) {
                try {

                    Image background = Image.getInstance(getImg("sfondi_pdf.png"));
                    float width = document.getPageSize().getWidth();
                    float height = document.getPageSize().getHeight();
                    writer.getDirectContentUnder().addImage(background, width, 0, 0, height, 0, 0);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }




    private void renderAll(Document document,  PdfWriter writer ) throws Exception{

        int status = ColumnText.START_COLUMN;
        Image img;
        int i=0;

        for (Kontakt kontakti : kontaktet) {

            if(progEvent!=null)progEvent.progress(i, kontaktet.size());

            i++;

            if(kontakti.getThumbnail_uri()!=null && kontakti.getThumbnail_uri().trim().length()>0) {
                img = Image.getInstance(getContactBitmapFromURI(Uri.parse(kontakti.getThumbnail_uri())));
            }else{
                img = Image.getInstance(getImg("noavatar.png"));
            }
            img.scaleToFit(60, 60);

            PdfPTable table = new PdfPTable(8);
            table.setTotalWidth(500);
            table.setLockedWidth(true);
            table.setWidths(new float[]{1, 1, 1, 1, 1, 1,1,1});


            Font font1 = FontFactory.getFont("Times-Roman", 20, Font.BOLD);

            PdfPCell cell;
            //foto
            cell = new PdfPCell();
            cell.addElement(img);
            cell.setRowspan(2);
            table.addCell(cell);
            //emri
            cell = new PdfPCell(new Phrase(kontakti.getEmri(), font1));
            cell.setPadding(10);
            cell.setColspan(3);
            cell.setRowspan(1);
            table.addCell(cell);
            //note
            cell = new PdfPCell();
            cell.setPadding(2);
            cell.addElement(new Phrase("Note:"));
            if(kontakti.getNotex()!=null) {
                cell.addElement(new Phrase(kontakti.getNotex()));
            }
            cell.setColspan(4);
            cell.setRowspan(2);
            table.addCell(cell);

            //company
            String comStr;
            if(kontakti.getEmri_organizates()!=null && kontakti.getPozita_organizate()!=null) {
                comStr = "" + kontakti.getEmri_organizates() + " : " + kontakti.getPozita_organizate();
            }else if(kontakti.getEmri_organizates()!=null){
                comStr = "" + kontakti.getPozita_organizate();
            }else{
                comStr = "";
            }
            cell = new PdfPCell(new Phrase(comStr));
            cell.setPadding(2);
            cell.setColspan(3);
            cell.setRowspan(1);
            table.addCell(cell);

            //statike /////////////////////
            table.addCell(getStaticCell("ik_tel.png", "telephone"));
            table.addCell(getStaticCell("ik_email.png", "emails"));
            table.addCell(getStaticCell("ik_address.png", "address"));
            table.addCell(getStaticCell("ik_im.png", "im"));


            Font font2 = FontFactory.getFont("Times-Roman", 12, Font.NORMAL);


            ///////////////////////////////////
            //nr e tel

            cell = new PdfPCell();
            cell.setColspan(2);
            cell.setRowspan(1);

            if(kontakti.getTelefonat().size()>0){
                List telList = new List(List.UNORDERED);
                for(KontaktTelefoni tel: kontakti.getTelefonat()){
                    telList.add(new ListItem( tel.getEmri() + ":" + tel.getNumri() , font2));
                }
                cell.addElement(telList);
            }else{
                cell.addElement(new Paragraph("No number", font2));
            }
            table.addCell(cell);


            /////////////ermailet//////////////

            cell = new PdfPCell();
            cell.setColspan(2);
            cell.setRowspan(1);

            if(kontakti.getEmailet().size()>0){
                List telList = new List(List.UNORDERED);
                KontaktEmail fnd = kontakti.getEmailet().get(kontakti.getEmailet().size()-1);
                for(KontaktEmail em: kontakti.getEmailet()){
                    telList.add(new ListItem( em.getEmri() + ":" + em.getAdresa() , font2));
                }
                cell.addElement(telList);
            }else{
                cell.addElement(new Paragraph("No email", font2));
            }
            table.addCell(cell);

            ////////////adresta ///////////////////

            cell = new PdfPCell();
            cell.setColspan(2);
            cell.setRowspan(1);

            if(kontakti.getAdresat().size()>0){
                Utils.log("nr adresa:" + kontakti.getAdresat().size());
                List telList = new List(List.UNORDERED);
                for(KontaktAdresa adresax: kontakti.getAdresat()){
                    String adrexazz = adresax.getAdresax();
                    Utils.log("adrexazz:" + adrexazz);
                    if(adrexazz!=null)telList.add(new ListItem( adrexazz , font2));
                }
                cell.addElement(telList);
//                cell.addElement(new Paragraph("toto - adresat", font2));
            }else{
                cell.addElement(new Paragraph("No address", font2));
            }

            table.addCell(cell);


            ///////////ims///////////
            cell = new PdfPCell();
            cell.setColspan(2);
            cell.setRowspan(1);

            if(kontakti.getMessengeret().size()>0){
                List telList = new List(List.UNORDERED);
                for(KontaktMessenger em: kontakti.getMessengeret()){
                    telList.add(new ListItem( em.getEmri() + ":" + em.getAdresa() , font2));
                }
                cell.addElement(telList);
            }else{
                cell.addElement(new Paragraph("No im", font2));
            }
            table.addCell(cell);


            /////// the end ////////////

            table.setSpacingAfter(20);

            document.add(table);

        }

    }

    private PdfPCell getStaticCell(String img_name, String txt) throws Exception{
        PdfPCell cell = new PdfPCell();
        cell.setColspan(2);
        cell.setRowspan(1);
        PdfPTable mainTable1 = new PdfPTable(2);
        mainTable1.setWidths(new float[]{1,4});
        Image ikona = Image.getInstance(getImg(img_name));
        ikona.scaleToFit(20, 20);
        PdfPCell c1 = new PdfPCell(ikona);
        c1.setBorder(Rectangle.NO_BORDER);
        mainTable1.addCell(c1);
        PdfPCell c2 = new PdfPCell(new Phrase(txt));
        c2.setBorder(Rectangle.NO_BORDER);
        mainTable1.addCell(c2);
        cell.addElement(mainTable1);
        return cell;
    }


    private void renderFundit(Document document,  PdfWriter writer ) throws  Exception{
        Image img;
        PdfPCell cell;
        PdfPTable table = new PdfPTable(6);
        for (Kontakt kontakti: kontaktet) {
            if(kontakti.getThumbnail_uri()!=null && kontakti.getThumbnail_uri().trim().length()>0) {
                img = Image.getInstance(getContactBitmapFromURI(Uri.parse(kontakti.getThumbnail_uri())));
                img.scaleToFit(80, 1000);
                cell = new PdfPCell(img, true);
                cell.setBorder(PdfPCell.NO_BORDER);
                table.addCell(cell);
            }
        }
        document.add(table);
    }

    private void renderKopertina(Document document,  PdfWriter writer ) throws Exception{

        Image img = Image.getInstance(getImg("kopertina.png"));
        img.setAbsolutePosition(0,0);
        document.add(img);

        //Now do the usual addition of text atop the template

        Font font1 = FontFactory.getFont("Times-Roman", 20, Font.BOLD);
        Font font2 = FontFactory.getFont("Times-Roman", 16, Font.BOLD);
        Font font3 = FontFactory.getFont("Times-Roman", 14, Font.NORMAL);

        Paragraph kp1 = new Paragraph("ADDRESS BOOK", font1);
        kp1.setAlignment(Element.ALIGN_CENTER);


        Paragraph kp2 = new Paragraph(android.os.Build.MODEL + "", font2);
        kp2.setAlignment(Element.ALIGN_CENTER);

        Paragraph kp3 = new Paragraph( kontaktet.size() + " contacts", font3);
        kp3.setAlignment(Element.ALIGN_CENTER);

        kp1.setSpacingAfter(20);
        kp2.setSpacingAfter(20);

        ColumnText ct = new ColumnText(writer.getDirectContent());
        ct.setSimpleColumn(192,358,404,490);

        ct.addElement(kp1);
        ct.addElement(kp2);
        ct.addElement(kp3);

        ct.go();

        Font font4 = FontFactory.getFont("Times-Roman", 12, Font.BOLD);
        Paragraph kp4 = new Paragraph( DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG).format(new Date()) + "", font4);
        kp4.setAlignment(Element.ALIGN_CENTER);
        kp4.setSpacingBefore(650);
        document.add(kp4);

        document.newPage();
    }

}
