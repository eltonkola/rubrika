package com.aldroid.rubrika.renderer;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.aldroid.rubrika.model.Kontakt;
import com.aldroid.rubrika.model.KontaktTelefoni;
import com.aldroid.rubrika.task.RendererTaskEvents;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.ZapfDingbatsList;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Elton on 07/09/2014.
 */
public class PdfSimple {

    private ArrayList<Kontakt> kontaktet= new ArrayList<Kontakt>();
    private String filename;
    private Context context;
    private RendererTaskEvents progEvent;

    public void createPdf(String filename, ArrayList<Kontakt> kontaktet, Context context, RendererTaskEvents progEvent) throws IOException, DocumentException, Exception {
        this.filename=filename;
        this.kontaktet=kontaktet;
        this.progEvent = progEvent;
        this.context=context;


        //Setup a new PDF Document
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filename));
        writer.setPageEvent(new Watermark());
        document.setMargins(20, 20, 100, 20);
        document.open();
        document.newPage();

        //hapim template pdf nga assets
        AssetManager assManager = context.getAssets();

         try {

            PdfContentByte cb = writer.getDirectContent();
            //krijo kopertinen
            renderKopertina(document, writer);
            //krijo permbajtjen
            document.add(createFirstTable());
            //fund
            document.close();

        } catch (IOException e) {
            e.printStackTrace();
            throw  new Exception();
        }

    }

    private byte[] getImg(String name){
        try {
            InputStream ims = context.getAssets().open(name);
            Bitmap bmp = BitmapFactory.decodeStream(ims);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            return stream.toByteArray();
        }catch (Exception e){
            //e.printStackTrace();
            return new byte[0];
        }

    }

    /**
     * Inner class to add a watermark to every page.
     */
    class Watermark extends PdfPageEventHelper {

        Font FONT = new Font(Font.FontFamily.HELVETICA, 52, Font.BOLD, new GrayColor(0.75f));

        public void onEndPage(PdfWriter writer, Document document) {
            //ColumnText.showTextAligned(writer.getDirectContentUnder(), com.itextpdf.text.Element.ALIGN_CENTER, new Phrase("my addres book", FONT), 297.5f, 421, writer.getPageNumber() % 2 == 1 ? 45 : -45);
            if(document.getPageNumber()>1) {
                try {

                    Image background = Image.getInstance(getImg("sfondi_pdf.png"));
                    float width = document.getPageSize().getWidth();
                    float height = document.getPageSize().getHeight();
                    writer.getDirectContentUnder().addImage(background, width, 0, 0, height, 0, 0);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Creates our first table
     * @return our first table
     */
    public PdfPTable createFirstTable() throws  Exception{
        // a table with three columns
        PdfPTable table = new PdfPTable(3);
        table.setTotalWidth(500);
        table.setLockedWidth(true);
        table.setWidths(new float[]{1, 4,4});
        // the cell object
        PdfPCell cell;
        // we add a cell with colspan 3
        cell = new PdfPCell(new Phrase("Phone contacts"));
        cell.setColspan(3);
        table.addCell(cell);

        for(int i=0;i<kontaktet.size(); i++) {

            if(progEvent!=null)progEvent.progress(i, kontaktet.size());

            Kontakt kontakti = kontaktet.get(i);
            Image img;
            if(kontakti.getThumbnail_uri()!=null && kontakti.getThumbnail_uri().trim().length()>0) {
                img = Image.getInstance(getContactBitmapFromURI(Uri.parse(kontakti.getThumbnail_uri())));
            }else{
                img = Image.getInstance(getImg("noavatar.png"));
            }
            img.scaleToFit(20, 20);
            table.addCell(img);

//            cell.setPadding(10);
            table.addCell("Emri:" + kontakti.getEmri());

            if(kontakti.getTelefonat().size()>0){
                ZapfDingbatsList telList = new ZapfDingbatsList(47, 45);
                KontaktTelefoni fnd = kontakti.getTelefonat().get(kontakti.getTelefonat().size()-1);
                for(KontaktTelefoni tel: kontakti.getTelefonat()){
                    telList.add(new ListItem( tel.getEmri() + ":" + tel.getNumri() ));
                }
                PdfPCell cellx = new PdfPCell();
                cellx.addElement(telList);
                table.addCell(cellx);
            }else{
                table.addCell("No number");
            }

        }

        return table;
    }

    public byte[] getContactBitmapFromURI(Uri uri) {
        try {
            InputStream ims  = context.getContentResolver().openInputStream(uri);
            Bitmap bmp = BitmapFactory.decodeStream(ims);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            return stream.toByteArray();
        }catch (Exception e){
            return new byte[0];
        }
    }


    private void renderKopertina(Document document,  PdfWriter writer ) throws Exception{

        Image img = Image.getInstance(getImg("kopertina.png"));
        img.setAbsolutePosition(0,0);
        document.add(img);

        //Now do the usual addition of text atop the template

        Font font1 = FontFactory.getFont("Times-Roman", 20, Font.BOLD);
        Font font2 = FontFactory.getFont("Times-Roman", 16, Font.BOLD);
        Font font3 = FontFactory.getFont("Times-Roman", 14, Font.NORMAL);

        Paragraph kp1 = new Paragraph("ADDRESS BOOK", font1);
        kp1.setAlignment(Element.ALIGN_CENTER);


        Paragraph kp2 = new Paragraph(android.os.Build.MODEL + "", font2);
        kp2.setAlignment(Element.ALIGN_CENTER);

        Paragraph kp3 = new Paragraph( kontaktet.size() + " contacts", font3);
        kp3.setAlignment(Element.ALIGN_CENTER);

        kp1.setSpacingAfter(20);
        kp2.setSpacingAfter(20);

        ColumnText ct = new ColumnText(writer.getDirectContent());
        ct.setSimpleColumn(192,358,404,490);

        ct.addElement(kp1);
        ct.addElement(kp2);
        ct.addElement(kp3);

        ct.go();

        Font font4 = FontFactory.getFont("Times-Roman", 12, Font.BOLD);
        Paragraph kp4 = new Paragraph( DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG).format(new Date()) + "", font4);
        kp4.setAlignment(Element.ALIGN_CENTER);
        kp4.setSpacingBefore(650);
        document.add(kp4);

        document.newPage();
    }

}
