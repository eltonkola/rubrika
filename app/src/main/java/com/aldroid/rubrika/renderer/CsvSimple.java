package com.aldroid.rubrika.renderer;

import android.content.Context;

import com.aldroid.rubrika.model.Kontakt;
import com.aldroid.rubrika.task.RendererTaskEvents;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * Created by Elton on 07/09/2014.
 */
public class CsvSimple {

    private ArrayList<Kontakt> kontaktet= new ArrayList<Kontakt>();
    private String filename;
    private RendererTaskEvents progEvent;

    public void createFile(String filename, ArrayList<Kontakt> kontaktet, RendererTaskEvents progEvent, Context context) throws IOException , Exception{
        this.filename=filename;
        this.kontaktet=kontaktet;
        this.progEvent = progEvent;

        CSVWriter writer = new CSVWriter(new FileWriter(filename));
        writer.writeNext(new String[]{"Name","Phone nr"});
        for(int i=0;i<kontaktet.size(); i++) {
            if(progEvent!=null)progEvent.progress(i, kontaktet.size());
            Kontakt kontakti = kontaktet.get(i);
            String labelTelefoni;
            if(kontakti.getTelefonat().size()>0){
                labelTelefoni = "" + kontakti.getTelefonat().get(0).getNumri();
            }else{
                labelTelefoni = "?";
            }
            writer.writeNext(new String[]{kontakti.getEmri(),labelTelefoni});
        }
        writer.close();

    }
}