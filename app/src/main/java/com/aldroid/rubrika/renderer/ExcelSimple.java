package com.aldroid.rubrika.renderer;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.aldroid.rubrika.model.Kontakt;
import com.aldroid.rubrika.task.RendererTaskEvents;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 * Created by Elton on 07/09/2014.
 */
public class ExcelSimple {

    private ArrayList<Kontakt> kontaktet= new ArrayList<Kontakt>();
    private String filename;
    private RendererTaskEvents progEvent;

    public void createExcel(String filename, ArrayList<Kontakt> kontaktet, RendererTaskEvents progEvent, Context context) throws IOException , Exception{
        this.filename=filename;
        this.kontaktet=kontaktet;
        this.progEvent = progEvent;

        WritableWorkbook workbook = Workbook.createWorkbook(new File(filename));

        WritableSheet sheet = workbook.createSheet("Rubrika - All Contacts", 0);

        AssetManager assManager = context.getAssets();
        InputStream isa = null;
        try {
            isa = assManager.open("sfondi_pdf.png");
            byte[] fileBytes=new byte[isa.available()];
            isa.read( fileBytes);
            isa.close();
            sheet.addImage(new WritableImage(0,0,2,1,fileBytes));
        }catch (Exception e){
            e.printStackTrace();
        }

        sheet.addCell(new Label(0, 1, "Name"));
        sheet.addCell(new Label(1, 1, "Phone nr"));
        sheet.addCell(new Label(2, 1, "Picture"));

        for(int i=0;i<kontaktet.size(); i++) {

            if(progEvent!=null)progEvent.progress(i, kontaktet.size());

            Kontakt kontakti = kontaktet.get(i);
            Label labelEmri = new Label(0, i+2, "" + kontakti.getEmri());

            Label labelTelefoni;
            if(kontakti.getTelefonat().size()>0){
                labelTelefoni = new Label(1, i+2, "" + kontakti.getTelefonat().get(0).getNumri());
            }else{
                labelTelefoni = new Label(1, i+2, "?");
            }

            sheet.addCell(labelEmri);
            sheet.addCell(labelTelefoni);

            //shtojme imazhin nese e ka

            if(kontakti.getThumbnail_uri()!=null && kontakti.getThumbnail_uri().trim().length()>0){
                Bitmap b = getContactBitmapFromURI(context, Uri.parse(kontakti.getThumbnail_uri()));
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                b.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                sheet.addImage(new WritableImage(2,i+2,1,1,byteArray));
            }

        }

        // All sheets and cells added. Now write out the workbook
        workbook.write();
        workbook.close();

    }

    public static Bitmap getContactBitmapFromURI(Context context, Uri uri) {
        try {
            InputStream input = context.getContentResolver().openInputStream(uri);
            if (input == null) {
                return null;
            }
            return BitmapFactory.decodeStream(input);
        }catch (Exception e){
            return null;
        }
    }

}