package com.aldroid.rubrika.renderer;

import android.content.Context;

import com.aldroid.rubrika.model.Kontakt;
import com.aldroid.rubrika.model.KontaktAdresa;
import com.aldroid.rubrika.model.KontaktEmail;
import com.aldroid.rubrika.model.KontaktMessenger;
import com.aldroid.rubrika.model.KontaktTelefoni;
import com.aldroid.rubrika.task.RendererTaskEvents;
import com.aldroid.rubrika.utils.Utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * Created by Elton on 07/09/2014.
 */
public class CsvComplete {

    private ArrayList<Kontakt> kontaktet= new ArrayList<Kontakt>();
    private String filename;
    private RendererTaskEvents progEvent;

    public void createFile(String filename, ArrayList<Kontakt> kontaktet, RendererTaskEvents progEvent, Context context) throws IOException , Exception{
        this.filename=filename;
        this.kontaktet=kontaktet;
        this.progEvent = progEvent;

        CSVWriter writer = new CSVWriter(new FileWriter(filename));
        writer.writeNext(new String[]{"Name","Phone nr", "Email", "Address", "Im", "Organization", "Notes"});

        for(int i=0;i<kontaktet.size(); i++) {
         if(progEvent!=null)progEvent.progress(i, kontaktet.size());
            Kontakt kontakti = kontaktet.get(i);

            String labelTelefoni;
            if(kontakti.getTelefonat().size()>0){
                String phonex = "";
                String poxFixx = "";
                if(kontakti.getTelefonat().size()>1)poxFixx= " | ";
                KontaktTelefoni fnd = kontakti.getTelefonat().get(kontakti.getTelefonat().size()-1);
                for(KontaktTelefoni tel: kontakti.getTelefonat()){
                    if(tel == fnd)poxFixx="";
                    if(tel.getNumri()!=null && tel.getNumri().trim().length()>0) {
                        phonex += " " + tel.getEmri() + "-" + tel.getNumri() + poxFixx;
                    }else{
                        phonex += " " + tel.getNumri()+ " / " + poxFixx;
                    }
                }
                labelTelefoni = phonex;
            }else{
                labelTelefoni = " ";
            }

            //email
            String labelEmail;
            if(kontakti.getEmailet().size()>0){
                String emailx = "";
                String poxFixx = "";
                if(kontakti.getEmailet().size()>1)poxFixx= " | ";
                KontaktEmail fnd = kontakti.getEmailet().get(kontakti.getEmailet().size()-1);
                for(KontaktEmail em: kontakti.getEmailet()){
                    if(em == fnd)poxFixx="";
                    if(em.getEmri()!=null && em.getEmri().trim().length()>0) {
                        emailx += " " + em.getEmri() + "-" + em.getAdresa()+ poxFixx;
                    }else{
                        emailx += " " + em.getAdresa()+ poxFixx;
                    }
                }
                labelEmail = emailx;
            }else{
                labelEmail = " ";
            }


            //address

            String labelAddress;
            if(kontakti.getAdresat().size()>0){
                String adrx = "";
                String poxFixx = "";
                if(kontakti.getAdresat().size()>1)poxFixx= " | ";
                KontaktAdresa fnd = kontakti.getAdresat().get(kontakti.getAdresat().size()-1);
                for(KontaktAdresa ad: kontakti.getAdresat()){
                    if(ad == fnd)poxFixx="";
                    if(ad.getCountry()!=null && ad.getState()!=null && ad.getCity()!=null && ad.getStreet()!=null && ad.getPoBox()!=null && ad.getPostalCode()!=null){
                        adrx += " " + ad.getCountry() + " " + ad.getState()+ " " + ad.getCity() + " " + ad.getStreet() + " " + ad.getPoBox() + " " + ad.getPostalCode() + poxFixx;
                    }else if(ad.getState()!=null && ad.getCity()!=null && ad.getStreet()!=null && ad.getPoBox()!=null && ad.getPostalCode()!=null){
                        adrx += " " + ad.getState()+ " " + ad.getCity() + " " + ad.getStreet() + " " + ad.getPoBox() + " " + ad.getPostalCode() + poxFixx;
                    }else if(ad.getCity()!=null && ad.getStreet()!=null && ad.getPoBox()!=null && ad.getPostalCode()!=null){
                        adrx += " " + ad.getCity() + " " + ad.getStreet() + " " + ad.getPoBox() + " " + ad.getPostalCode() + poxFixx;
                    }else if(ad.getStreet()!=null && ad.getPoBox()!=null && ad.getPostalCode()!=null){
                        adrx += " " + ad.getStreet() + " " + ad.getPoBox() + " " + ad.getPostalCode() + poxFixx;
                    }
//                    else if(ad.getPoBox()!=null && ad.getPostalCode()!=null){
//                        adrx += " " + ad.getPoBox() + " " + ad.getPostalCode() + poxFixx;
//                    }if(ad.getPostalCode()!=null){
//                        adrx += " " + ad.getPostalCode() + poxFixx;
//                    }
                    else{
                        Utils.log("no address found for:" + kontakti.getEmri());
                    }
                }
                labelAddress = adrx;
            }else{
                labelAddress = " ";
            }
            //im

            String labelIm;
            if(kontakti.getMessengeret().size()>0){
                String adrx = "";
                String poxFixx = "";
                if(kontakti.getAdresat().size()>1)poxFixx= " | ";
                KontaktMessenger fnd = kontakti.getMessengeret().get(kontakti.getMessengeret().size()-1);
                for(KontaktMessenger im: kontakti.getMessengeret()){
                    if(im == fnd)poxFixx="";
                    if(im.getEmri()!=null && im.getEmri().trim().length()>0) {
                        adrx += " " + im.getEmri() + "-" + im.getAdresa() + poxFixx;
                    }else{
                        adrx += " " + im.getAdresa()+ poxFixx;
                    }
                }
                labelIm = adrx;
            }else{
                labelIm = " ";
            }

            //organization
            String orgz="";
            if(kontakti.getEmri_organizates()!=null && kontakti.getPozita_organizate()!=null) {
                orgz=  "" + kontakti.getEmri_organizates() + " : " + kontakti.getPozita_organizate();
            }else if(kontakti.getEmri_organizates()!=null){
                orgz = "" + kontakti.getPozita_organizate();
            }

            //note
            String notexx="";
            if(kontakti.getNotex()!=null){
                notexx = kontakti.getNotex();
            }

            writer.writeNext(new String[]{kontakti.getEmri(),labelTelefoni, labelEmail, labelAddress ,labelIm, orgz, notexx});
        }
        writer.close();
    }
}
