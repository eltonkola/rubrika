package com.aldroid.rubrika.renderer;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.aldroid.rubrika.model.Kontakt;
import com.aldroid.rubrika.model.KontaktAdresa;
import com.aldroid.rubrika.model.KontaktEmail;
import com.aldroid.rubrika.model.KontaktMessenger;
import com.aldroid.rubrika.model.KontaktTelefoni;
import com.aldroid.rubrika.task.RendererTaskEvents;
import com.aldroid.rubrika.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 * Created by Elton on 07/09/2014.
 */
public class ExcelComplete {


    private ArrayList<Kontakt> kontaktet= new ArrayList<Kontakt>();
    private String filename;
    private RendererTaskEvents progEvent;

    public void createExcel(String filename, ArrayList<Kontakt> kontaktet, RendererTaskEvents progEvent, Context context) throws IOException , Exception{
        this.filename=filename;
        this.kontaktet=kontaktet;
        this.progEvent = progEvent;

        WritableWorkbook workbook = Workbook.createWorkbook(new File(filename));

        WritableSheet sheet = workbook.createSheet("Rubrika - All Contacts", 0);

        AssetManager assManager = context.getAssets();
        InputStream isa = null;
        try {
            isa = assManager.open("sfondi_pdf.png");
            byte[] fileBytes=new byte[isa.available()];
            isa.read( fileBytes);
            isa.close();
            sheet.addImage(new WritableImage(0,0,2,1,fileBytes));
        }catch (Exception e){
            e.printStackTrace();
        }

        sheet.addCell(new Label(0, 1, "Picture"));
        sheet.addCell(new Label(1, 1, "Name"));
        sheet.addCell(new Label(2, 1, "Phone nr"));
        sheet.addCell(new Label(3, 1, "Email"));
        sheet.addCell(new Label(4, 1, "Address"));
        sheet.addCell(new Label(5, 1, "Im"));
        sheet.addCell(new Label(6, 1, "Organization"));
        sheet.addCell(new Label(7, 1, "Notes"));


        for(int i=0;i<kontaktet.size(); i++) {

            if(progEvent!=null)progEvent.progress(i, kontaktet.size());

            Kontakt kontakti = kontaktet.get(i);
            //foto
            if(kontakti.getThumbnail_uri()!=null && kontakti.getThumbnail_uri().trim().length()>0){
                Bitmap b = getContactBitmapFromURI(context, Uri.parse(kontakti.getThumbnail_uri()));
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                b.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                sheet.addImage(new WritableImage(0,i+2,1,1,byteArray));
            }
            //name
            sheet.addCell(new Label(1, i+2, "" + kontakti.getEmri()));
            //phone
            Label labelTelefoni;
            if(kontakti.getTelefonat().size()>0){
                String phonex = "";
                String poxFixx = "";
                if(kontakti.getTelefonat().size()>1)poxFixx= " / ";
                KontaktTelefoni fnd = kontakti.getTelefonat().get(kontakti.getTelefonat().size()-1);
                for(KontaktTelefoni tel: kontakti.getTelefonat()){
                    if(tel == fnd)poxFixx="";
                    if(tel.getNumri()!=null && tel.getNumri().trim().length()>0) {
                        phonex += " " + tel.getEmri() + "-" + tel.getNumri() + poxFixx;
                    }else{
                        phonex += " " + tel.getNumri()+ " / " + poxFixx;
                    }
                }
                labelTelefoni = new Label(2, i+2, phonex);
            }else{
                labelTelefoni = new Label(2, i+2, " ");
            }
            sheet.addCell(labelTelefoni);
            //email
            Label labelEmail;
            if(kontakti.getEmailet().size()>0){
                String emailx = "";
                String poxFixx = "";
                if(kontakti.getEmailet().size()>1)poxFixx= " / ";
                KontaktEmail fnd = kontakti.getEmailet().get(kontakti.getEmailet().size()-1);
                for(KontaktEmail em: kontakti.getEmailet()){
                    if(em == fnd)poxFixx="";
                    if(em.getEmri()!=null && em.getEmri().trim().length()>0) {
                        emailx += " " + em.getEmri() + "-" + em.getAdresa()+ poxFixx;
                    }else{
                        emailx += " " + em.getAdresa()+ poxFixx;
                    }
                }
                labelEmail = new Label(3, i+2, emailx);
            }else{
                labelEmail = new Label(3, i+2, " ");
            }
            sheet.addCell(labelEmail);
            //address

            Label labelAddress;
            if(kontakti.getAdresat().size()>0){
                String adrx = "";
                String poxFixx = "";
                if(kontakti.getAdresat().size()>1)poxFixx= " / ";
                KontaktAdresa fnd = kontakti.getAdresat().get(kontakti.getAdresat().size()-1);
                for(KontaktAdresa ad: kontakti.getAdresat()){
                    if(ad == fnd)poxFixx="";
                    if(ad.getCountry()!=null && ad.getState()!=null && ad.getCity()!=null && ad.getStreet()!=null && ad.getPoBox()!=null && ad.getPostalCode()!=null){
                        adrx += " " + ad.getCountry() + " " + ad.getState()+ " " + ad.getCity() + " " + ad.getStreet() + " " + ad.getPoBox() + " " + ad.getPostalCode() + poxFixx;
                    }else if(ad.getState()!=null && ad.getCity()!=null && ad.getStreet()!=null && ad.getPoBox()!=null && ad.getPostalCode()!=null){
                        adrx += " " + ad.getState()+ " " + ad.getCity() + " " + ad.getStreet() + " " + ad.getPoBox() + " " + ad.getPostalCode() + poxFixx;
                    }else if(ad.getCity()!=null && ad.getStreet()!=null && ad.getPoBox()!=null && ad.getPostalCode()!=null){
                        adrx += " " + ad.getCity() + " " + ad.getStreet() + " " + ad.getPoBox() + " " + ad.getPostalCode() + poxFixx;
                    }else if(ad.getStreet()!=null && ad.getPoBox()!=null && ad.getPostalCode()!=null){
                        adrx += " " + ad.getStreet() + " " + ad.getPoBox() + " " + ad.getPostalCode() + poxFixx;
                    }
//                    else if(ad.getPoBox()!=null && ad.getPostalCode()!=null){
//                        adrx += " " + ad.getPoBox() + " " + ad.getPostalCode() + poxFixx;
//                    }if(ad.getPostalCode()!=null){
//                        adrx += " " + ad.getPostalCode() + poxFixx;
//                    }
                    else{
                        Utils.log("no address found for:" + kontakti.getEmri());
                    }
                }
                labelAddress = new Label(4, i+2, adrx);
            }else{
                labelAddress = new Label(4, i+2, " ");
            }
            sheet.addCell(labelAddress);

            //im

            Label labelIm;
            if(kontakti.getMessengeret().size()>0){
                String adrx = "";
                String poxFixx = "";
                if(kontakti.getAdresat().size()>1)poxFixx= " / ";
                KontaktMessenger fnd = kontakti.getMessengeret().get(kontakti.getMessengeret().size()-1);
                for(KontaktMessenger im: kontakti.getMessengeret()){
                    if(im == fnd)poxFixx="";
                    if(im.getEmri()!=null && im.getEmri().trim().length()>0) {
                        adrx += " " + im.getEmri() + "-" + im.getAdresa() + poxFixx;
                    }else{
                        adrx += " " + im.getAdresa()+ poxFixx;
                    }
                }
                labelIm = new Label(5, i+2, adrx);
            }else{
                labelIm = new Label(5, i+2, " ");
            }
            sheet.addCell(labelIm);

            //organization
            if(kontakti.getEmri_organizates()!=null && kontakti.getPozita_organizate()!=null) {
                sheet.addCell(new Label(6, i + 2, "" + kontakti.getEmri_organizates() + " : " + kontakti.getPozita_organizate()));
            }else if(kontakti.getEmri_organizates()!=null){
                sheet.addCell(new Label(6, i + 2, "" + kontakti.getPozita_organizate()));
            }else{
                Utils.log("no organization found for:" + kontakti.getEmri());
            }

            //note
            if(kontakti.getNotex()!=null){
                sheet.addCell(new Label(7, i + 2, "" + kontakti.getNotex()));
            }else{
                Utils.log("no notes found for:" + kontakti.getNotex());
            }

        }

        // All sheets and cells added. Now write out the workbook
        workbook.write();
        workbook.close();

    }

    public static Bitmap getContactBitmapFromURI(Context context, Uri uri) {
        try {
            InputStream input = context.getContentResolver().openInputStream(uri);
            if (input == null) {
                return null;
            }
            return BitmapFactory.decodeStream(input);
        }catch (Exception e){
            return null;
        }
    }

}
